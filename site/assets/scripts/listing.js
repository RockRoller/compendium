'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function getFullSkinIndex(url = "/search.json") {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const response = yield fetch(url);
            if (response.status != 200) {
                throw new Error(`Search fetch failed: ${response.status}`);
            }
            return JSON.parse(yield response.text(), function (key, value) {
                if (["date_added", "date_released"].includes(key)) {
                    return new Date(value);
                }
                else if (["game_modes", "author", "resolutions", "ratios", "tags", "categories", "skin_collection"].includes(key)) {
                    if (value == null) {
                        return [];
                    }
                }
                return value;
            });
        }
        catch (e) {
            console.error(e.message);
        }
    });
}
function getPrestigeMap() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const response = yield fetch("/prestige.json");
            if (response.status != 200) {
                throw new Error(`Search fetch failed: ${response.status}`);
            }
            return JSON.parse(yield response.text());
        }
        catch (e) {
            console.error(e.message);
        }
    });
}

const searchDebounceTime = 1000;
const resultsPerPage = 18;
const storageKeys = {
    searchFolded: "search-extended",
};
const searchDates = {
    oldest: new Date("2008-01-01"),
    newest: new Date(Date.now()),
};
let prestigeMap = {};
function initPrestigeMap() {
    return __awaiter(this, void 0, void 0, function* () {
        prestigeMap = (yield getPrestigeMap());
    });
}

const chevronLeft = `<svg class="icon chevron-left" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>`;
const chevronRight = `<svg class="icon chevron-right" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>`;
const cross = `<svg class="icon cross" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>`;
const fullscreen = `<svg class="icon fullscreen" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4"></path></svg>`;
const taiko = `<svg class="icon taiko" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.37686 0 0 5.37686 0 12C0 18.6232 5.37686 24 12 24C18.6231 24 24 18.6232 24 12C24.0178 5.37686 18.641 0 12 0ZM12 21.9526C6.51632 21.9526 2.04748 17.4837 2.04748 12C2.04748 6.51633 6.51632 2.04748 12 2.04748C17.4836 2.04748 21.9525 6.51633 21.9525 12C21.9525 17.5015 17.5015 21.9526 12 21.9526Z"></path><path d="M12.0003 4.34426C7.76293 4.34426 4.32672 7.78046 4.32672 12.0178C4.32672 16.2553 7.76293 19.6915 12.0003 19.6915C16.2377 19.6915 19.6739 16.2553 19.6739 12.0178C19.6739 7.76266 16.2377 4.34426 12.0003 4.34426ZM6.89052 12.0001C6.89052 9.61429 8.5285 7.62022 10.7362 7.05049V16.9496C8.5285 16.3798 6.89052 14.3858 6.89052 12.0001ZM13.2822 16.9496V7.05049C15.4899 7.62022 17.1101 9.61429 17.1101 12.0001C17.1101 14.3858 15.4899 16.3798 13.2822 16.9496Z"></path></svg>`;
const mania = `<svg class="icon mania" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.37686 0 0 5.37686 0 12C0 18.6232 5.37686 24 12 24C18.6231 24 24 18.6232 24 12C24.0178 5.37686 18.641 0 12 0ZM12 21.9526C6.51632 21.9526 2.04748 17.4837 2.04748 12C2.04748 6.51633 6.51632 2.04748 12 2.04748C17.4836 2.04748 21.9525 6.51633 21.9525 12C21.9525 17.5015 17.5015 21.9526 12 21.9526Z"></path><path d="M12.0001 4.273C11.288 4.273 10.7182 4.84274 10.7182 5.55491V18.4451C10.7182 19.1573 11.288 19.727 12.0001 19.727C12.7123 19.727 13.282 19.1573 13.282 18.4451V5.55491C13.282 4.84274 12.7123 4.273 12.0001 4.273Z"></path><path d="M16.1124 7.88724C15.4002 7.88724 14.8305 8.45697 14.8305 9.16915V14.8131C14.8305 15.5253 15.4002 16.095 16.1124 16.095C16.8245 16.095 17.3943 15.5253 17.3943 14.8131V9.18694C17.3943 8.47478 16.8245 7.88724 16.1124 7.88724Z"></path><path d="M7.90508 7.90498C7.19291 7.90498 6.62318 8.47471 6.62318 9.18688V14.8308C6.62318 15.543 7.19291 16.1127 7.90508 16.1127C8.61724 16.1127 9.18698 15.543 9.18698 14.8308V9.18688C9.18698 8.47471 8.61724 7.90498 7.90508 7.90498Z"></path></svg>`;
const ctb = `<svg class="icon catch" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 18.6231 5.37686 24 12 24C18.6232 24 24 18.6231 24 12C24 5.37688 18.6232 4.4322e-05 12 4.4322e-05C5.37686 -0.017807 0 5.35903 0 12ZM21.9526 12C21.9526 17.4837 17.4837 21.9525 12 21.9525C6.51633 21.9525 2.04748 17.4837 2.04748 12C2.04748 6.51635 6.51633 2.04747 12 2.04747C17.5015 2.04747 21.9526 6.4985 21.9526 12Z"></path><path d="M13.0681 12.0178C13.0681 13.0861 13.9405 13.9585 15.0087 13.9585C16.077 13.9585 16.9494 13.0861 16.9494 12.0178C16.9494 10.9496 16.077 10.0772 15.0087 10.0772C13.9227 10.095 13.0681 10.9496 13.0681 12.0178Z"></path><path d="M12.4808 15.8991C12.4808 14.8273 11.6119 13.9584 10.5401 13.9584C9.46831 13.9584 8.59945 14.8273 8.59945 15.8991C8.59945 16.9709 9.46831 17.8397 10.5401 17.8397C11.6119 17.8397 12.4808 16.9709 12.4808 15.8991Z"></path><path d="M12.4808 8.10091C12.4808 7.02911 11.6119 6.16026 10.5401 6.16026C9.4683 6.16026 8.59944 7.02911 8.59944 8.10091C8.59944 9.1727 9.4683 10.0416 10.5401 10.0416C11.6119 10.0416 12.4808 9.1727 12.4808 8.10091Z"></path></svg>`;
const standard = `<svg class="icon standard" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M4.32678 12.0178C4.32678 7.78046 7.76299 4.34425 12.0003 4.34425C16.2378 4.34425 19.674 7.76266 19.674 12.0178C19.674 16.2553 16.2378 19.6915 12.0003 19.6915C7.76299 19.6915 4.32678 16.2553 4.32678 12.0178Z"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM0 12C0 5.37258 5.37258 0 12 0C18.6274 0 24 5.37258 24 12C24 18.6274 18.6274 24 12 24C5.37258 24 0 18.6274 0 12Z"></path></svg>`;
const documentSolid = `<svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd"></path></svg>`;
const image = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><rect x="32" y="48" width="192" height="160" rx="8" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></rect><path d="M32,168l50.3-50.3a8,8,0,0,1,11.4,0l44.6,44.6a8,8,0,0,0,11.4,0l20.6-20.6a8,8,0,0,1,11.4,0L224,184" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path><circle cx="156" cy="100" r="12"></circle></svg>`;
const arrowUp = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><polyline points="94.1 121.9 128 88 161.9 121.9" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline><line x1="128" y1="168" x2="128" y2="88" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const arrowDown = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><polyline points="94.1 134.1 128 168 161.9 134.1" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline><line x1="128" y1="88" x2="128" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const trash = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><line x1="216" y1="56" x2="40" y2="56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="104" y1="104" x2="104" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="152" y1="104" x2="152" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><path d="M200,56V208a8,8,0,0,1-8,8H64a8,8,0,0,1-8-8V56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path><path d="M168,56V40a16,16,0,0,0-16-16H104A16,16,0,0,0,88,40V56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path></svg>`;
const add = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><line x1="88" y1="128" x2="168" y2="128" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="128" y1="88" x2="128" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const playSolid = `<svg fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm14.024-.983a1.125 1.125 0 010 1.966l-5.603 3.113A1.125 1.125 0 019 15.113V8.887c0-.857.921-1.4 1.671-.983l5.603 3.113z" fill-rule="evenodd"></path></svg>`;

var icons = /*#__PURE__*/Object.freeze({
    __proto__: null,
    chevronLeft: chevronLeft,
    chevronRight: chevronRight,
    cross: cross,
    fullscreen: fullscreen,
    taiko: taiko,
    mania: mania,
    ctb: ctb,
    standard: standard,
    documentSolid: documentSolid,
    image: image,
    arrowUp: arrowUp,
    arrowDown: arrowDown,
    trash: trash,
    add: add,
    playSolid: playSolid
});

function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: className }));
    Object.assign(element.style, style);
    element.append(...children);
    return element;
}
function ensuredSelector(query) {
    const element = document.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}
class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}
function createIcon(name) {
    return createElementFromHTML(icons[name]);
}
function createElementFromHTML(html) {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild;
}

function randomiseArray(items) {
    for (let i = items.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [items[i], items[j]] = [items[j], items[i]];
    }
    return items.sort(() => Math.random() - 0.5);
}

function setUrlQueryParameters(filter) {
    if (isFilterDefault(filter)) {
        clearUrlQuery();
        return;
    }
    let newUrl = window.location.pathname.concat("?", generateUrlQueryParameter(filter).toString());
    window.history.pushState(null, document.title, newUrl);
    window.history.replaceState(null, document.title, newUrl);
}
function generateUrlQueryParameter(filter) {
    const searchParam = new URLSearchParams();
    if (filter.order != "new-old") {
        searchParam.append("order", filter.order);
    }
    if (filter.gameModeInclude.length != 0) {
        searchParam.append("modes", filter.gameModeInclude.join("+"));
    }
    if (filter.strInclude.length != 0) {
        searchParam.append("search", filter.strInclude.join("+"));
    }
    if (filter.catExclude.length != 0) {
        searchParam.append("catex", filter.catExclude.join("+"));
    }
    if (filter.catInclude.length != 0) {
        searchParam.append("catin", filter.catInclude.join("+"));
    }
    if (filter.resolutions.length != 0) {
        searchParam.append("res", filter.resolutions.join("+"));
    }
    if (filter.ratios.length != 0) {
        searchParam.append("ratio", filter.ratios.join("+"));
    }
    if (filter.dateAfter.getTime() != searchDates.oldest.getTime()) {
        searchParam.append("date-after", filter.dateAfter.getTime().toString());
    }
    if (filter.dateBefore.getTime() != searchDates.newest.getTime()) {
        searchParam.append("date-before", filter.dateBefore.getTime().toString());
    }
    return searchParam;
}
function updateUrlQueryPage(page) {
    const searchParam = new URLSearchParams(window.location.search);
    const pageKey = "p";
    if (searchParam.has(pageKey)) {
        if (page != 1) {
            searchParam.set(pageKey, page.toString());
        }
        else {
            searchParam.delete(pageKey);
        }
    }
    else if (page != 1) {
        searchParam.append(pageKey, page.toString());
    }
    let newUrl = window.location.pathname.concat("?", searchParam.toString());
    if (newUrl.endsWith("?")) {
        newUrl = newUrl.replace("?", "");
    }
    window.history.pushState(null, document.title, newUrl);
    window.history.replaceState(null, document.title, newUrl);
}
function clearUrlQuery() {
    window.history.pushState(null, document.title, window.location.pathname);
    window.history.replaceState(null, document.title, window.location.pathname);
}
function readUrlQuery() {
    const searchParam = new URLSearchParams(window.location.search);
    let filter = {
        strInclude: [],
        gameModeInclude: [],
        catInclude: [],
        catExclude: [],
        resolutions: [],
        ratios: [],
        order: ensuredSelector(".filter-order-selector").value,
        dateAfter: searchDates.oldest,
        dateBefore: searchDates.newest,
    };
    let page = 1;
    const legacyCategories = ["vtuber", "love_live", "vocaloid", "touhou", "azur_lane", "kpop"];
    searchParam.forEach((value, key) => {
        switch (key) {
            case "order":
                filter.order = value;
                break;
            case "res":
                value.split("+").forEach((e) => filter.resolutions.push(e));
                break;
            case "ratio":
                value.split("+").forEach((e) => filter.ratios.push(e));
                break;
            case "search":
                value.split("+").forEach((e) => filter.strInclude.push(e));
                break;
            case "catex":
            case "tagex":
                value.split("+").forEach((e) => {
                    if (!legacyCategories.includes(e)) {
                        filter.catExclude.push(e);
                    }
                });
                break;
            case "catin":
            case "tagin":
                value.split("+").forEach((e) => {
                    if (!legacyCategories.includes(e)) {
                        filter.catInclude.push(e);
                    }
                    else {
                        if (!filter.strInclude) {
                            filter.strInclude = [];
                        }
                        filter.strInclude.push(e);
                    }
                });
                break;
            case "modes":
                value.split("+").forEach((e) => filter.gameModeInclude.push(e));
                break;
            case "date-before":
                filter.dateBefore = new Date(parseInt(value));
                break;
            case "date-after":
                filter.dateAfter = new Date(parseInt(value));
                break;
        }
    });
    if (searchParam.has("p")) {
        page = parseInt(searchParam.get("p"));
    }
    return [filter, page];
}
function useReadQuery(filter, page, settings) {
    const searchInput = document.querySelector("#search-input");
    const tagIncludeNodes = document.querySelectorAll(".filter-tag--cat");
    const tagExcludeNodes = document.querySelectorAll(".filter-tag--cat");
    const resolutionIncludeNodes = document.querySelectorAll(".filter-tag--resolution");
    const ratioIncludeNodes = document.querySelectorAll(".filter-tag--ratio");
    const gameModeIncludeNodes = document.querySelectorAll(".filter-game-mode");
    const orderSelector = document.querySelector(".filter-order-selector");
    searchInput.value = filter.strInclude.join(" ");
    filter.gameModeInclude.forEach((e) => {
        gameModeIncludeNodes.forEach((g) => {
            if (g.dataset.tag == e) {
                g.classList.add("include");
            }
        });
    });
    filter.resolutions.forEach((e) => {
        resolutionIncludeNodes.forEach((r) => {
            if (r.dataset.tag == e) {
                r.classList.add("include");
            }
        });
    });
    filter.ratios.forEach((e) => {
        ratioIncludeNodes.forEach((r) => {
            if (r.dataset.tag == e) {
                r.classList.add("include");
            }
        });
    });
    filter.catInclude.forEach((e) => {
        tagIncludeNodes.forEach((t) => {
            if (t.dataset.tag == e) {
                t.classList.add("include");
            }
        });
    });
    filter.catExclude.forEach((e) => {
        tagExcludeNodes.forEach((t) => {
            if (t.dataset.tag == e) {
                t.classList.add("exclude");
            }
        });
    });
    if (filter.order != "new-old") {
        orderSelector.querySelector(`option[value=${filter.order}]`).selected = true;
    }
    if (filter.dateAfter.getTime() != searchDates.oldest.getTime()) {
        ensuredSelector("#date-after").value = filter.dateAfter.toISOString().split("T")[0];
    }
    if (filter.dateBefore.getTime() != searchDates.newest.getTime()) {
        ensuredSelector("#date-before").value = filter.dateBefore.toISOString().split("T")[0];
    }
    performSearch(settings, page);
}

class Pagination extends HTMLElement {
    constructor(chunkedSkinList, page = 1, filter) {
        super();
        this.paginationTextNodes = [];
        this.paginationRange = 2;
        this.chunkedSkinList = chunkedSkinList;
        this.filter = filter;
        this.currentChunkIndex = page - 1;
        updateUrlQueryPage(page);
        this.classList.add("pagination", "search-pagination");
        this.first = this.createIcon("first", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = 0;
            this.handleNavigationClick();
        });
        this.prev = this.createIcon("prev", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = this.currentChunkIndex == 0 ? 0 : this.currentChunkIndex - 1;
            this.handleNavigationClick();
        });
        this.append(this.first, this.prev);
        this.chunkedSkinList.forEach((skinChunk, i) => {
            this.paginationTextNodes.push(this.createPaginationTextNode(i, generateUrlQueryParameter(this.filter)));
        });
        this.next = this.createIcon("next", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = this.currentChunkIndex == this.chunkedSkinList.length - 1 ? this.chunkedSkinList.length - 1 : this.currentChunkIndex + 1;
            this.handleNavigationClick();
        });
        this.last = this.createIcon("last", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = chunkedSkinList.length - 1;
            this.handleNavigationClick();
        });
        this.append(this.next, this.last);
        this.showPaginationNodes();
        this.updatePaginationVisualState();
        renderSkinChunk(this.chunkedSkinList[this.currentChunkIndex]);
    }
    showPaginationNodes() {
        let firstPage = this.currentChunkIndex - this.paginationRange < 0 ? 0 : this.currentChunkIndex - this.paginationRange;
        let lastPage = this.currentChunkIndex + this.paginationRange;
        if (firstPage == 0) {
            lastPage = this.paginationRange * 2;
        }
        if (firstPage == 1) {
            lastPage = 1 + this.paginationRange * 2;
        }
        if (lastPage == this.chunkedSkinList.length) {
            firstPage = lastPage - this.paginationRange * 2 - 1;
        }
        if (lastPage > this.chunkedSkinList.length) {
            lastPage = this.chunkedSkinList.length;
            firstPage = lastPage - 1 - this.paginationRange * 2;
        }
        if (firstPage < 0) {
            firstPage = 0;
        }
        this.querySelectorAll("a:not(.pagination-item__icon-node)").forEach((e) => e.remove());
        this.paginationTextNodes
            .slice(firstPage, lastPage + 1)
            .reverse()
            .forEach((e) => this.insertBefore(e, this.children[2]));
    }
    createIcon(icon, queryParam, eventFunction) {
        const icons = {
            first: `<svg class="icon " fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7"></path></svg>`,
            prev: `<svg class="icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>`,
            next: `<svg class="icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>`,
            last: `<svg class="icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7"></path></svg>`,
        };
        switch (icon) {
            case "prev":
                if (this.currentChunkIndex != (0 )) {
                    queryParam.append("p", this.currentChunkIndex.toString());
                }
                else {
                    queryParam.delete("p");
                }
                break;
            case "next":
                if (this.currentChunkIndex + 1 == this.chunkedSkinList.length) {
                    queryParam.set("p", this.chunkedSkinList.length.toString());
                }
                else {
                    queryParam.append("p", (this.currentChunkIndex + 2).toString());
                }
                break;
            case "last":
                queryParam.append("p", this.chunkedSkinList.length.toString());
                break;
        }
        return createElement("a", {
            className: `pagination-item pagination-item__icon-node`,
            children: [
                createElement("div", {
                    style: {
                        display: "flex",
                    },
                    attributes: {
                        innerHTML: icons[icon],
                    },
                }),
            ],
            attributes: {
                href: window.location.pathname.concat("?", queryParam.toString()),
                onclick: (e) => {
                    e.preventDefault();
                    eventFunction();
                },
            },
        });
    }
    handleNavigationClick() {
        if (this.chunkedSkinList[this.currentChunkIndex] != undefined) {
            renderSkinChunk(this.chunkedSkinList[this.currentChunkIndex]);
            this.updatePaginationVisualState();
            this.showPaginationNodes();
            updateUrlQueryPage(this.currentChunkIndex + 1);
            const prevParam = new URLSearchParams(new URL(this.prev.href).search);
            if (prevParam.has("p")) {
                if (this.currentChunkIndex != 0) {
                    prevParam.set("p", this.currentChunkIndex.toString());
                }
                else {
                    prevParam.delete("p");
                }
            }
            else if (this.currentChunkIndex != 1) {
                prevParam.append("p", (this.currentChunkIndex + 1).toString());
            }
            this.prev.href = window.location.pathname.concat("?", prevParam.toString());
            const nextParam = new URLSearchParams(new URL(this.next.href).search);
            if (nextParam.has("p")) {
                if (this.currentChunkIndex + 1 == this.chunkedSkinList.length) {
                    nextParam.set("p", this.chunkedSkinList.length.toString());
                }
                else {
                    nextParam.set("p", (this.currentChunkIndex + 2).toString());
                }
            }
            this.next.href = window.location.pathname.concat("?", nextParam.toString());
        }
    }
    createPaginationTextNode(index, queryParam) {
        if (index != 0) {
            queryParam.append("p", (index + 1).toString());
        }
        return createElement("a", {
            className: `pagination-item ${index == 0 ? "current-page" : ""}`,
            attributes: {
                innerText: (index + 1).toString(),
                onclick: (e) => {
                    e.preventDefault();
                    this.currentChunkIndex = index;
                    this.handleNavigationClick();
                },
                href: window.location.pathname.concat("?", queryParam.toString()),
            },
        });
    }
    updatePaginationVisualState() {
        Array.from(this.children).forEach((e) => e.classList.remove("current-page"));
        if (this.currentChunkIndex == 0) {
            this.first.classList.add("current-page");
            this.prev.classList.add("current-page");
        }
        if (this.currentChunkIndex == this.chunkedSkinList.length - 1 || this.chunkedSkinList.length == 0) {
            this.next.classList.add("current-page");
            this.last.classList.add("current-page");
        }
        const element = this.paginationTextNodes[this.currentChunkIndex];
        if (element !== null && element !== undefined) {
            element.classList.add("current-page");
        }
    }
}
customElements.define("search-pagination", Pagination);

class SkinItem extends HTMLElement {
    constructor(skin) {
        super();
        this.skin = skin;
        this.classList.add("skin-item");
        this.append(createElement("a", {
            className: "skin-item__link",
            attributes: {
                href: skin.url,
            },
        }));
        this.append(this.createCover());
        this.append(this.createInformationArea());
        this.append(this.createTagsArea());
    }
    createCover() {
        const cover = createElement("div", {
            className: "skin-item__cover",
            children: [
                createElement("img", {
                    className: "cover",
                    attributes: {
                        src: `/assets/img/covers/${this.skin.forum_thread_id}.webp`,
                    },
                }),
            ],
        });
        const versionCount = this.skin.skin_collection.length;
        if (versionCount > 1) {
            cover.append(createElement("div", {
                className: "skin-item__cover-versions",
                children: [
                    createIcon("documentSolid"),
                    createElement("span", {
                        className: "version-count",
                        attributes: {
                            innerText: versionCount.toString(),
                        },
                    }),
                ],
            }));
        }
        return cover;
    }
    createInformationArea() {
        return createElement("div", {
            className: "skin-item__information",
            children: [
                createElement("div", {
                    className: "skin-item__information-row",
                    children: [
                        createElement("span", {
                            className: "skin-name",
                            attributes: {
                                innerText: this.skin.skin_name,
                            },
                        }),
                        createElement("div", {
                            className: "game-modes",
                            children: this.skin.game_modes.map((mode) => createIcon(mode == "catch" ? "ctb" : mode)),
                        }),
                    ],
                }),
                createElement("div", {
                    className: "skin-item__information-row",
                    children: [
                        createElement("span", {
                            className: "author",
                            attributes: {
                                innerText: this.skin.formatted_author,
                            },
                        }),
                        createElement("span", {
                            className: "information-seperator",
                        }),
                        ...this.skin.resolutions.map((e) => {
                            return createElement("span", {
                                className: "resolution",
                                attributes: {
                                    innerText: e.toUpperCase(),
                                },
                            });
                        }),
                    ],
                }),
            ],
        });
    }
    createTagsArea() {
        const tags = this.generateTagsData().map((tag) => createTag(tag));
        return createElement("div", {
            className: "skin-item__tags-list-container",
            children: [
                createElement("div", {
                    className: "tags-list",
                    children: tags,
                }),
            ],
        });
        function createTag(tag) {
            return createElement("a", {
                className: `tag${tag.prestige != "" ? " tag--prestige tag--prestige-" + tag.prestige : ""}`,
                attributes: {
                    innerText: tag.tag.replace(/_/g, " "),
                    href: `?${tag.searchKey}=${tag.tag}`,
                },
            });
        }
    }
    generateTagsData() {
        const tagsData = [];
        this.skin.resolutions.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "res",
                tag: e.toUpperCase(),
            });
        });
        this.skin.ratios.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "ratio",
                tag: e,
            });
        });
        this.skin.tags.sort().forEach((e) => {
            tagsData.push({
                prestige: prestigeMap[e],
                searchKey: "search",
                tag: e,
            });
        });
        this.skin.categories.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "tagin",
                tag: e,
            });
        });
        return tagsData;
    }
}
customElements.define("skin-item", SkinItem);

function paginateSearchResults(skinList, page, filter, settings) {
    const chunkSize = settings.pagination ? resultsPerPage : skinList.length;
    const chunkedSkinList = chunkSkinList(skinList, chunkSize);
    hideDefaultListing();
    const pagination = document.querySelector(".search-pagination");
    if (pagination !== null) {
        pagination.remove();
    }
    document.querySelector("#skin-grid-results").after(new Pagination(chunkedSkinList, page, filter));
}
function chunkSkinList(skinList, chunkSize) {
    const chunkedList = [];
    for (let i = 0; i < skinList.length; i += chunkSize) {
        chunkedList.push(skinList.slice(i, i + chunkSize));
    }
    return chunkedList;
}
function renderSkinChunk(skinList) {
    clearResultGrid();
    if (skinList == undefined) {
        return;
    }
    const resultGrid = document.querySelector("#skin-grid-results");
    skinList.forEach((s) => resultGrid.append(new SkinItem(s)));
}
function clearResultGrid() {
    document.querySelector("#skin-grid-results").innerHTML = "";
}

function getFilters() {
    const searchInput = ensuredSelector("#search-input");
    const tagIncludeNodes = document.querySelectorAll(".filter-tag--cat.include");
    const tagExcludeNodes = document.querySelectorAll(".filter-tag--cat.exclude");
    const resolutionIncludeNodes = document.querySelectorAll(".filter-tag--resolution.include");
    const ratioIncludeNodes = document.querySelectorAll(".filter-tag--ratio.include");
    const gameModeIncludeNodes = document.querySelectorAll(".filter-game-mode.include");
    const dateAfterInput = ensuredSelector("#date-after");
    const dateBeforeInput = ensuredSelector("#date-before");
    const exactSearchRegex = /"[^"]*"/gm;
    let filter = {
        strInclude: [],
        gameModeInclude: Array.from(gameModeIncludeNodes).map((e) => e.dataset.tag),
        catInclude: Array.from(tagIncludeNodes).map((e) => e.dataset.tag),
        catExclude: Array.from(tagExcludeNodes).map((e) => e.dataset.tag),
        resolutions: Array.from(resolutionIncludeNodes).map((e) => e.dataset.tag),
        ratios: Array.from(ratioIncludeNodes).map((e) => e.dataset.tag),
        order: document.querySelector(".filter-order-selector").value,
        dateAfter: dateAfterInput.value == "" ? searchDates.oldest : new Date(dateAfterInput.value),
        dateBefore: dateBeforeInput.value == "" ? searchDates.newest : new Date(dateBeforeInput.value),
    };
    if (searchInput.value != "") {
        let search = searchInput.value.toLowerCase();
        const match = search.match(exactSearchRegex);
        if (match !== null) {
            match.forEach((e) => {
                search = search.replace(e, "");
                addStringSearchElements([e]);
            });
        }
        addStringSearchElements(search.trim().split(" "));
    }
    return filter;
    function addStringSearchElements(elements) {
        elements.forEach((e) => {
            if (e != "") {
                filter.strInclude.push(e);
            }
        });
    }
}
function filterSkins(skinIndex, filter) {
    const filteredSkins = skinIndex.filter((skin) => {
        let skinCategories = skin.categories.map((e) => e.toLowerCase());
        let skinGameModes = skin.game_modes;
        let skinResolutions = skin.resolutions;
        let skinRatios = skin.ratios;
        let skinMetaData = [...skin.author, ...skinCategories, ...skin.tags, skin.skin_name, ...skin.skin_collection.map((e) => e.name)]
            .join(" ")
            .toLowerCase()
            .replace(/[^a-zA-Z0-9 ]/g, "");
        return (!filter.catExclude.some((i) => skinCategories.includes(i)) &&
            filter.gameModeInclude.every((i) => skinGameModes.includes(i)) &&
            filter.catInclude.every((i) => skinCategories.includes(i)) &&
            filter.resolutions.every((i) => skinResolutions.includes(i)) &&
            (skinRatios.includes("all") ? true : filter.ratios.every((i) => skinRatios.includes(i))) &&
            filter.strInclude.every((i) => skinMetaData.includes(i.replace(/[^a-zA-Z0-9 ]/g, ""))) &&
            filter.dateAfter < skin.date_released &&
            filter.dateBefore > skin.date_released);
    });
    switch (filter.order) {
        case "a-z":
            return filteredSkins.sort((a, b) => {
                if (a.skin_name.toLowerCase() < b.skin_name.toLowerCase())
                    return -1;
                if (a.skin_name.toLowerCase() > b.skin_name.toLowerCase())
                    return 1;
                return 0;
            });
        case "z-a":
            return filteredSkins.sort((a, b) => {
                if (a.skin_name.toLowerCase() < b.skin_name.toLowerCase())
                    return 1;
                if (a.skin_name.toLowerCase() > b.skin_name.toLowerCase())
                    return -1;
                return 0;
            });
        case "old-new":
            return filteredSkins.sort((a, b) => {
                if (a.date_released > b.date_released)
                    return 1;
                if (a.date_released < b.date_released)
                    return -1;
                return 0;
            });
        case "new-old":
            return filteredSkins.sort((a, b) => {
                if (a.date_released < b.date_released)
                    return 1;
                if (a.date_released > b.date_released)
                    return -1;
                return 0;
            });
        case "comp-new-old":
            return filteredSkins.sort((a, b) => {
                if (a.date_added < b.date_added)
                    return 1;
                if (a.date_added > b.date_added)
                    return -1;
                return 0;
            });
        case "comp-old-new":
            return filteredSkins.sort((a, b) => {
                if (a.date_added > b.date_added)
                    return 1;
                if (a.date_added < b.date_added)
                    return -1;
                return 0;
            });
        case "random":
            return randomiseArray(filteredSkins);
        default:
            return filteredSkins;
    }
}
function performSearch(settings, page = 1) {
    const resultNumberDisplayJs = document.querySelector(".filter-results-count.js");
    const filter = getFilters();
    setUrlQueryParameters(filter);
    if (isFilterDefault(filter)) {
        showDefaultListing();
        return;
    }
    const filteredSkinList = filterSkins(settings.skinIndex, filter);
    resultNumberDisplayJs.innerText = `${filteredSkinList.length.toString()} skins`;
    paginateSearchResults(filteredSkinList, page, filter, settings);
}
function hideDefaultListing() {
    var _a;
    ensuredSelector("#skin-grid").classList.add("hidden");
    (_a = document.querySelector(".pagination:not(.search-pagination)")) === null || _a === void 0 ? void 0 : _a.classList.add("hidden");
    ensuredSelector(".filter-results-count.default").classList.add("hidden");
    ensuredSelector(".filter-results-count.js").classList.remove("hidden");
}
function showDefaultListing() {
    var _a;
    clearResultGrid();
    ensuredSelector(".search-pagination").remove();
    ensuredSelector(".filter-results-count.js").classList.add("hidden");
    ensuredSelector("#skin-grid").classList.remove("hidden");
    ensuredSelector(".filter-results-count.default").classList.remove("hidden");
    (_a = document.querySelector(".pagination:not(.search-pagination)")) === null || _a === void 0 ? void 0 : _a.classList.remove("hidden");
}
function isFilterDefault(filter) {
    if (filter.order == "new-old" &&
        filter.gameModeInclude.length != 0 &&
        filter.strInclude.length != 0 &&
        filter.catExclude.length != 0 &&
        filter.catInclude.length != 0 &&
        filter.resolutions.length != 0 &&
        filter.ratios.length != 0) {
        return true;
    }
    return false;
}

function addEventListeners(settings) {
    const searchDebounce = debounce((skinIndex) => {
        performSearch(settings);
    }, searchDebounceTime);
    addClearListener();
    addSearchFoldInListener();
    addVisualsTagsListeners();
    addVisualsGameModeListeners();
    addInputSearchListeners(settings.skinIndex, searchDebounce);
}
function debounce(func, wait) {
    let timeoutID;
    return function (...args) {
        clearTimeout(timeoutID);
        const context = this;
        timeoutID = window.setTimeout(function () {
            func.apply(context, args);
        }, wait);
    };
}
function addInputSearchListeners(skinIndex, searchDebounce) {
    [
        ...document.querySelectorAll(".filter-game-mode"),
        ...document.querySelectorAll(".filter-tag--cat"),
        ...document.querySelectorAll(".filter-tag--resolution"),
        ...document.querySelectorAll(".filter-tag--ratio"),
    ].forEach((e) => {
        e.addEventListener("click", () => {
            searchDebounce(skinIndex);
        });
    });
    [ensuredSelector("#search-input"), ensuredSelector("#date-after"), ensuredSelector("#date-before")].forEach((e) => {
        e.addEventListener("input", () => {
            searchDebounce(skinIndex);
        });
    });
    ensuredSelector(".filter-order-selector").addEventListener("change", () => {
        searchDebounce(skinIndex);
    });
}
function addVisualsTagsListeners() {
    document.querySelectorAll(".filter-tag--cat").forEach((e) => {
        e.addEventListener("click", () => {
            if (e.classList.contains("include")) {
                e.classList.replace("include", "exclude");
            }
            else if (e.classList.contains("exclude")) {
                e.classList.remove("exclude");
            }
            else {
                e.classList.add("include");
            }
        });
    });
    document.querySelectorAll(".filter-tag--resolution").forEach((e) => {
        e.addEventListener("click", () => e.classList.toggle("include"));
    });
    document.querySelectorAll(".filter-tag--ratio").forEach((e) => {
        e.addEventListener("click", () => e.classList.toggle("include"));
    });
}
function addVisualsGameModeListeners() {
    document.querySelectorAll(".filter-game-mode").forEach((e) => {
        e.addEventListener("click", () => {
            e.classList.contains("include") ? e.classList.remove("include") : e.classList.add("include");
        });
    });
}
function addClearListener() {
    ensuredSelector(".trash").addEventListener("click", () => {
        document.querySelector("#search-input").value = "";
        document.querySelector(".filter-order-selector option").selected = true;
        ensuredSelector("#date-after").value = "";
        ensuredSelector("#date-before").value = "";
        clearUrlQuery();
        showDefaultListing();
        document.querySelectorAll(".include").forEach((e) => e.classList.remove("include"));
        document.querySelectorAll(".exclude").forEach((e) => e.classList.remove("exclude"));
    });
}
function addSearchFoldInListener() {
    ensuredSelector(".search .chevron-down").addEventListener("click", () => {
        ensuredSelector(".filters").classList.toggle("folded");
        const setting = localStorage.getItem(storageKeys.searchFolded);
        localStorage.setItem(storageKeys.searchFolded, setting == "true" ? "false" : "true");
    });
}
function readSearchState() {
    if (localStorage.getItem(storageKeys.searchFolded) == null) {
        localStorage.setItem(storageKeys.searchFolded, "true");
    }
    const extended = localStorage.getItem(storageKeys.searchFolded);
    if (extended == "false") {
        ensuredSelector(".filters").classList.add("folded");
    }
}

function initSearch(settings) {
    return __awaiter(this, void 0, void 0, function* () {
        readSearchState();
        yield initPrestigeMap();
        if (window.location.search != "") {
            useReadQuery(...readUrlQuery(), settings);
        }
        addEventListeners(settings);
        performSearch(settings);
    });
}

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const skinIndex = yield getFullSkinIndex();
        initSearch({
            skinIndex: skinIndex,
            pagination: true,
        });
    });
}
