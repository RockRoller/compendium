'use strict';

function randomiseArray(items) {
    for (let i = items.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [items[i], items[j]] = [items[j], items[i]];
    }
    return items.sort(() => Math.random() - 0.5);
}

function ensuredSelector(query) {
    const element = document.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}
class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    const parent = ensuredSelector(".skin-grid");
    const skins = parent.querySelectorAll(".skin-grid .skin-item");
    skins.forEach((skin) => {
        parent.removeChild(skin);
    });
    parent.append(...randomiseArray(Array.from(skins)));
}
