'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function ensuredSelector(query) {
    const element = document.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}
class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        initEventListeners();
    });
}
function initEventListeners() {
    const noWarningCheckBox = ensuredSelector(".column-select__checkbox#show");
    document.querySelectorAll(".column-select__checkbox:not(#show)").forEach((e) => {
        e.addEventListener("change", () => {
            toggleColumn(e.id, e.checked);
            hideNoWarningRows(noWarningCheckBox.checked);
        });
    });
    noWarningCheckBox.addEventListener("change", () => {
        hideNoWarningRows(noWarningCheckBox.checked);
    });
}
function toggleColumn(column, show) {
    document.querySelectorAll(`.${column}`).forEach((e) => {
        show ? e.classList.remove("hidden") : e.classList.add("hidden");
    });
}
function hideNoWarningRows(show) {
    document.querySelectorAll("tbody tr").forEach((tr) => {
        if (show) {
            tr.classList.remove("hidden");
        }
        else {
            if (tr.querySelectorAll(".warning:not(.hidden)").length == 0) {
                tr.classList.add("hidden");
            }
            else {
                tr.classList.remove("hidden");
            }
        }
        document.querySelectorAll(".page-index").forEach((header) => {
            if (Array.from(header.nextElementSibling.children).every((child) => child.classList.contains("hidden"))) {
                header.classList.add("hidden");
            }
            else {
                header.classList.remove("hidden");
            }
        });
    });
}
