---
layout: skin

skin_name: "Remilia Scarlet"
forum_thread_id: 1548547
date_added: 2023-02-07
game_modes:
    - standard
    - mania
    - catch
author:
    - 11711165
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "remilia_scarlet"
    - "touhou_project"
categories:
    - anime
skin_collection:
    - name: "Remilia Scarlet"
      screenshots:
          - "https://i.imgur.com/WylpvgJ.png"
          - "https://i.imgur.com/I3DaM0M.png"
          - "https://i.imgur.com/pbNvisT.png"
          - "https://i.imgur.com/q0MteBN.png"
          - "https://i.imgur.com/g1wRL6j.png"
          - "https://i.imgur.com/dzj2UNt.png"
          - "https://i.imgur.com/bjPj3cu.png"
          - "https://i.imgur.com/dzj2UNt.png"
          - "https://i.imgur.com/rWWFgr2.png"
          - "https://i.imgur.com/rWWFgr2.png"
          - "https://i.imgur.com/crYtQV5.png"
          - "https://i.imgur.com/jWwEdTJ.png"
videos:
    skinship:
    author:
---
