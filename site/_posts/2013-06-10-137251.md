---
layout: skin

skin_name: "Light into Darkness"
forum_thread_id: 137251
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 614822
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Light into Darkness"
      screenshots:
          - "http://i.imgur.com/Gu9xUp0.jpg"
          - "http://i.imgur.com/tkLMUN9.jpg"
          - "http://i.imgur.com/wxob7RA.png"
          - "http://i.imgur.com/LAxfkpQ.png"
          - "http://i.imgur.com/5U80ods.png"
videos:
    skinship:
    author:
---
