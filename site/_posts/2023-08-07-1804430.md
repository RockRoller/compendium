---
layout: skin

skin_name: "My Little Pony X Rainbow High X Monster High"
forum_thread_id: 1804430
date_added: 2024-01-09
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 31921012
resolutions:
    - sd
ratios:
    - "all"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "my_little_pony"
    - "rainbow_high"
    - "monster_high"
categories:
    - other
skin_collection:
    - name: "My Little Pony X Rainbow High X Monster High"
      screenshots:
          - "https://i.imgur.com/amk9Igj.png"
          - "https://i.imgur.com/fZWF6mq.png"
          - "https://i.imgur.com/kPoFRZY.png"
          - "https://i.imgur.com/AjR2QMH.png"
          - "https://i.imgur.com/7gyADTM.png"
          - "https://i.imgur.com/WlFBrsQ.png"
          - "https://i.imgur.com/MaIHB9e.png"
videos:
    skinship:
    author:
---
