---
layout: skin

skin_name: "Pink Bean - MapleStory"
forum_thread_id: 1700282
date_added: 2023-01-05
game_modes:
    - standard
author:
    - 6785191
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "skinners'_bulletin_showcase"
    - "maple_story"
    - "pink_bean"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Pink Bean - MapleStory"
      screenshots:
          - "https://i.imgur.com/CFZtPB8.png"
          - "https://i.imgur.com/7GwM84z.png"
          - "https://i.imgur.com/ToJQzSB.png"
          - "https://i.imgur.com/uYuvsm0.png"
          - "https://i.imgur.com/829uU9f.png"
          - "https://i.imgur.com/wu6aMQo.png"
          - "https://i.imgur.com/XWCEbGq.png"
          - "https://i.imgur.com/XqYIR7C.png"
          - "https://i.imgur.com/Pi59mKH.png"
          - "https://i.imgur.com/uRygsZv.png"
          - "https://i.imgur.com/BRuOkal.png"
          - "https://i.imgur.com/5xeSaH6.png"
          - "https://i.imgur.com/huE8Qn3.png"
          - "https://i.imgur.com/VPHse8v.png"
videos:
    skinship:
        - m4QzX8uEKAo
    author:
---
