---
layout: skin

skin_name: "Air Gear"
forum_thread_id: 731170
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 9212147
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
    - "air_gear"
categories:
    - anime
skin_collection:
    - name: "Air Gear"
      screenshots:
          - "http://i.imgur.com/uhFuOlA.jpg"
          - "http://i.imgur.com/Sb5XpUz.jpg"
          - "http://i.imgur.com/ubLTz8V.jpg"
          - "http://i.imgur.com/JBh5eDS.jpg"
          - "http://i.imgur.com/CTnSiFr.jpg"
          - "http://i.imgur.com/xhfan2A.jpg"
          - "http://i.imgur.com/PtpkJRT.jpg"
          - "http://i.imgur.com/mebu1Rw.jpg"
videos:
    skinship:
    author:
---
