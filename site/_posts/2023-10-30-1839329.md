---
layout: skin

skin_name: "paper, pen, marker"
forum_thread_id: 1839329
date_added: 2023-12-17
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7082132
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "handdrawn"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "paper"
categories:
    - eyecandy
    - joke
skin_collection:
    - name: "paper, pen, marker"
      screenshots:
          - "https://i.imgur.com/oxJSdzC.jpg"
          - "https://i.imgur.com/cy7v68c.jpg"
          - "https://i.imgur.com/tbl0cRO.jpg"
          - "https://i.imgur.com/JNgAc1M.jpg"
          - "https://i.imgur.com/55k5g1F.jpg"
          - "https://i.imgur.com/KMsVFa0.jpg"
          - "https://i.imgur.com/i9tZD6R.jpg"
          - "https://i.imgur.com/2JrYXbG.jpg"
          - "https://i.imgur.com/tNmKw67.jpg"
          - "https://i.imgur.com/ZfFAiYh.jpg"
          - "https://i.imgur.com/GoOAGRt.jpg"
          - "https://i.imgur.com/qBp77ES.jpg"
          - "https://i.imgur.com/VMeObOx.jpg"
          - "https://i.imgur.com/q9aV8OY.jpg"
          - "https://i.imgur.com/ChDdSo4.jpg"
          - "https://i.imgur.com/nDyE1rk.jpg"
          - "https://i.imgur.com/lVs6YqL.jpg"
          - "https://i.imgur.com/3UfF1yd.jpg"
videos:
    skinship:
    author:
---
