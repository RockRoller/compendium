---
layout: skin

skin_name: "KIWI"
forum_thread_id: 1568423
date_added: 2022-05-30
game_modes:
    - standard
author:
    - 13015586
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "KIWI"
      screenshots:
          - "https://i.imgur.com/DdzJ8FR.jpg"
          - "https://i.imgur.com/rDnS9Fp.jpg"
          - "https://i.imgur.com/AFaEESp.jpg"
          - "https://i.imgur.com/C9AsIbf.jpg"
          - "https://i.imgur.com/0qMWggN.jpg"
          - "https://i.imgur.com/t4skXLv.jpg"
          - "https://i.imgur.com/L1kTP6f.jpg"
          - "https://i.imgur.com/LDOCJaq.jpg"
          - "https://i.imgur.com/d8r2hsf.jpg"
          - "https://i.imgur.com/rNcktf5.jpg"
videos:
    skinship:
    author:
---
