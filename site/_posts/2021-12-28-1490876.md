---
layout: skin

skin_name: "Shiratama;You"
forum_thread_id: 1490876
date_added: 2022-02-07
game_modes:
    - standard
author:
    - 5730417
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "shiratamaco"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Shiratama;You"
      screenshots:
          - "https://i.imgur.com/XykaiZ3.png"
          - "https://i.imgur.com/byS8OBO.png"
          - "https://i.imgur.com/OxtSU6X.png"
          - "https://i.imgur.com/HYqgewG.png"
          - "https://i.imgur.com/KtcfdIP.png"
          - "https://i.imgur.com/OfqmykJ.png"
          - "https://i.imgur.com/PvuPKOj.png"
          - "https://i.imgur.com/nSjaHAA.png"
          - "https://i.imgur.com/R3u4Ht5.png"
          - "https://i.imgur.com/sUxCWNW.png"
          - "https://i.imgur.com/BVjvt6d.png"
videos:
    skinship:
    author:
---
