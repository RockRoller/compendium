---
layout: skin

skin_name: "FOOL MOON NIGHT 2.0"
forum_thread_id: 1885740
date_added: 2024-03-03
game_modes:
    - standard
author:
    - 11805037
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "beatmap"
    - "a_fool_moon_night"
    - "the_koxx"
categories:
    - eyecandy
skin_collection:
    - name: "FOOL MOON NIGHT 2.0"
      screenshots:
          - "https://i.imgur.com/gveQFRe.jpg"
          - "https://i.imgur.com/0x54IMS.jpg"
          - "https://i.imgur.com/VlHCGeg.jpg"
          - "https://i.imgur.com/qVmHRv1.jpg"
          - "https://i.imgur.com/3zo34ER.jpg"
          - "https://i.imgur.com/IUExJhe.jpg"
          - "https://i.imgur.com/r2TY74V.jpg"
          - "https://i.imgur.com/XLiLLAq.jpg"
          - "https://i.imgur.com/WDHvdke.jpg"
          - "https://i.imgur.com/bJ68i17.jpg"
          - "https://i.imgur.com/yJRxYJD.jpg"
          - "https://i.imgur.com/Bu9iVQW.jpg"
          - "https://i.imgur.com/sxs61uf.jpg"
          - "https://i.imgur.com/YJmFFFc.jpg"
videos:
    skinship:
    author:
---
