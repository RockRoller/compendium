---
layout: skin

skin_name: "8ghts"
forum_thread_id: 727680
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
    - catch
author:
    - 11829195
resolutions:
    - sd
ratios:
    - "all"
tags:
categories:
    - other
skin_collection:
    - name: "8ghts"
      screenshots:
          - "https://i.imgur.com/fh5kC81.png"
          - "https://i.imgur.com/hFQCAW0.png"
          - "https://i.imgur.com/izhqwZJ.png"
          - "https://i.imgur.com/VWWXAZ6.png"
          - "https://i.imgur.com/TSCHSiV.png"
videos:
    skinship:
    author:
---
