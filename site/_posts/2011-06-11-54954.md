---
layout: skin

skin_name: "DJMax Osuka"
forum_thread_id: 54954
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 17894
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "DJMax Osuka"
      screenshots:
          - "http://i.imgur.com/y7HRp3W.jpg"
          - "http://i.imgur.com/2xPEPc2.jpg"
          - "http://i.imgur.com/dSqfLgH.jpg"
          - "http://i.imgur.com/8au94Mk.jpg"
          - "http://i.imgur.com/lDcaN50.jpg"
          - "http://i.imgur.com/MrmEjUp.jpg"
          - "http://i.imgur.com/cOFEuzn.jpg"
          - "http://i.imgur.com/jqI2mHF.jpg"
videos:
    skinship:
    author:
---
