---
layout: skin

skin_name: "Checkered Style"
forum_thread_id: 503119
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
author:
    - 9974
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Checkered Style"
      screenshots:
          - "http://imgur.com/46ZCYD1.png"
          - "http://imgur.com/8jxzrlw.png"
          - "http://imgur.com/oZkXdMw.png"
          - "http://imgur.com/UEFuXCC.png"
          - "http://i.imgur.com/4BPCGrl.png"
          - "http://imgur.com/8iyxQBN.png"
          - "http://imgur.com/X9V4jk9.png"
videos:
    skinship:
    author:
---
