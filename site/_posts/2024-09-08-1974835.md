---
layout: skin

skin_name: "Teto's Territory"
forum_thread_id: 1974835
date_added: 2024-09-08
game_modes:
    - standard
    - taiko
    - catch
author:
    - 34419864
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "kasane_teto"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Teto's Territory"
      screenshots:
          - "https://i.imgur.com/h3U1VpP.png"
          - "https://i.imgur.com/4BnVGHF.png"
          - "https://i.imgur.com/BiVOd43.png"
          - "https://i.imgur.com/MaYZLlN.png"
          - "https://i.imgur.com/BjVDnqF.png"
          - "https://i.imgur.com/uwhTLyU.png"
          - "https://i.imgur.com/WsTdBN4.png"
          - "https://i.imgur.com/l2xr86a.png"
          - "https://i.imgur.com/nfuBNoX.png"
          - "https://i.imgur.com/CYaKMAR.png"
          - "https://i.imgur.com/bebYKSb.png"
          - "https://i.imgur.com/CD3ddQx.png"
          - "https://i.imgur.com/L0aNOKP.png"
videos:
    skinship:
    author:
        - gzJqk4rzfvQ
        - k89UWliJMKU
---
