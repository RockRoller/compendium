---
layout: skin

skin_name: "Black & White"
forum_thread_id: 526891
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 8195163
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Black & White"
      screenshots:
          - "http://i.imgur.com/nWWQAr1.jpg"
          - "http://i.imgur.com/2hbSopn.jpg"
          - "http://i.imgur.com/kqOs2Q8.png"
          - "http://i.imgur.com/5r4FHD7.png"
          - "http://i.imgur.com/CU9b4xy.png"
          - "http://i.imgur.com/7ttAkNw.png"
videos:
    skinship:
    author:
---
