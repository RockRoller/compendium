---
layout: skin

skin_name: "Yukari Yakumo"
forum_thread_id: 1687654
date_added: 2023-01-25
game_modes:
    - standard
author:
    - 15581205
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "touhou_project"
    - "yukari_yakumo"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "Yukari Yakumo"
      screenshots:
          - "https://i.imgur.com/6hP9Pzb.png"
          - "https://i.imgur.com/f044es1.png"
          - "https://i.imgur.com/Sj5gt3j.png"
          - "https://i.imgur.com/SJ1vFSf.png"
          - "https://i.imgur.com/zGv5t0Z.png"
videos:
    skinship:
    author:
---
