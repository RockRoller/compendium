---
layout: skin

skin_name: "b4_simple"
forum_thread_id: 848576
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2127235
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - eyecandy
skin_collection:
    - name: "b4_simple"
      screenshots:
          - "https://i.imgur.com/PxD64PD.jpg"
          - "https://i.imgur.com/dBQB5uc.jpg"
          - "https://i.imgur.com/w6h2vhH.jpg"
          - "https://i.imgur.com/F3X63K7.jpg"
          - "https://i.imgur.com/zGK66hp.jpg"
          - "https://i.imgur.com/h6X8B3I.jpg"
          - "https://i.imgur.com/WKCsJb4.jpg"
          - "https://i.imgur.com/ErmnUHc.jpg"
videos:
    skinship:
    author:
---
