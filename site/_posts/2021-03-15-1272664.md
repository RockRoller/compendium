---
layout: skin

skin_name: "Multicolor Diamond HUD"
forum_thread_id: 1272664
date_added: 2021-07-15
game_modes:
    - standard
author:
    - 20767121
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
categories:
    - minimalistic
skin_collection:
    - name: "Multicolor Diamond HUD"
      screenshots:
          - "https://i.imgur.com/bt7Usdm.jpeg"
          - "https://i.imgur.com/5E5bj2g.jpeg"
          - "https://i.imgur.com/LJxqicA.jpeg"
          - "https://i.imgur.com/3DNhuRl.jpeg"
          - "https://i.imgur.com/o3O9095.jpeg"
          - "https://i.imgur.com/zDRNzYy.jpeg"
          - "https://i.imgur.com/bAfeFBs.jpeg"
          - "https://i.imgur.com/jKHM5gh.jpeg"
          - "https://i.imgur.com/psXSl8j.jpeg"
videos:
    skinship:
        - DIJ5Cg-yK6I
    author:
---
