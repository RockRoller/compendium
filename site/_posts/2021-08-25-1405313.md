---
layout: skin

skin_name: "Tomioka Giyū"
forum_thread_id: 1405313
date_added: 2021-08-31
game_modes:
    - standard
author:
    - 3858685
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "demon_slayer"
    - "kimetsu_no_yaiba"
    - "tomioka_giyu"
    - "skinners'_bulletin_showcase"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Tomioka Giyū"
      screenshots:
          - "https://i.imgur.com/6kY3DZ5.png"
          - "https://i.imgur.com/Rtomqz0.png"
          - "https://i.imgur.com/SlxK05o.png"
          - "https://i.imgur.com/jrUQR9g.png"
          - "https://i.imgur.com/4Agk3KB.png"
          - "https://i.imgur.com/l7KPh4T.png"
          - "https://i.imgur.com/kQsVDN1.png"
          - "https://i.imgur.com/QNDcNDA.png"
          - "https://i.imgur.com/qyhlynk.png"
          - "https://i.imgur.com/THDGLCo.png"
          - "https://i.imgur.com/t14Yrec.png"
videos:
    skinship:
        - l0L2Zroknso
    author:
---
