---
layout: skin

skin_name: "Aoba Suzukaze & Flower Style"
forum_thread_id: 678795
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
    - catch
author:
    - 5212789
    - 5730417
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "aoba_suzukaze"
    - "new_game!"
categories:
    - anime
skin_collection:
    - name: "Aoba Suzukaze & Flower Style"
      screenshots:
          - "http://i.imgur.com/Y015N3F.png"
          - "http://i.imgur.com/Xshwc8G.jpg"
          - "http://i.imgur.com/cugBBzW.jpg"
          - "http://i.imgur.com/Wt5prmv.png"
          - "http://i.imgur.com/gPTbwsE.png"
          - "http://i.imgur.com/YfpkwjG.jpg"
          - "http://i.imgur.com/Cldbh7v.jpg"
          - "http://i.imgur.com/V1mvNmr.jpg"
          - "http://i.imgur.com/n0ycVlN.png"
          - "http://i.imgur.com/d8iXMTl.jpg"
videos:
    skinship:
    author:
---
