---
layout: skin

skin_name: "Proximity"
forum_thread_id: 804307
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 9683505
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Proximity"
      screenshots:
          - "https://i.imgur.com/GGpMRYi.png"
          - "https://imgur.com/6AONcYv.png"
          - "https://i.imgur.com/vy48I7Z.png"
          - "https://i.imgur.com/OZn3aGG.png"
          - "https://i.imgur.com/Ml5llPn.png"
          - "https://i.imgur.com/FovZPuB.png"
          - "https://i.imgur.com/SychvLS.png"
          - "https://i.imgur.com/DNIVoR9.png"
          - "https://i.imgur.com/F2GH0V8.png"
          - "https://i.imgur.com/pDtC2oE.png"
          - "https://i.imgur.com/uzQUP6C.png"
          - "https://i.imgur.com/Elaa1di.png"
videos:
    skinship:
    author:
---
