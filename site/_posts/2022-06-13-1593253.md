---
layout: skin

skin_name: "Project M.E.P.H."
forum_thread_id: 1593253
date_added: 2022-06-22
game_modes:
    - standard
author:
    - 26853961
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Project M.E.P.H."
      screenshots:
          - "https://i.imgur.com/RwKzGgA.jpg"
          - "https://i.imgur.com/huC68St.jpg"
          - "https://i.imgur.com/wAsCVYJ.jpg"
          - "https://i.imgur.com/5bCFNOM.jpg"
          - "https://i.imgur.com/hhZdrzR.jpg"
          - "https://i.imgur.com/8BdaoMq.jpg"
          - "https://i.imgur.com/X65BIYV.jpg"
          - "https://i.imgur.com/uEG4BT2.jpg"
          - "https://i.imgur.com/37BA0QL.jpg"
          - "https://i.imgur.com/pqLrw7m.jpg"
videos:
    skinship:
    author:
---
