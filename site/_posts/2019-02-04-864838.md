---
layout: skin

skin_name: "Girls Frontline Skin"
forum_thread_id: 864838
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 1903966
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "girls'_frontline"
    - "springfield"
    - "g36"
    - "as_val"
    - "g36c"
categories:
    - anime
    - game
skin_collection:
    - name: "Girls Frontline Skin"
      screenshots:
          - "http://i.imgur.com/WtSKlXo.png"
          - "http://i.imgur.com/P00fsFf.png"
          - "http://i.imgur.com/L9vlnox.png"
          - "http://i.imgur.com/lMlfhrU.png"
          - "http://i.imgur.com/qpMztQc.png"
          - "http://i.imgur.com/hvxHBTq.png"
          - "http://i.imgur.com/95nXzwK.png"
videos:
    skinship:
    author:
---
