---
layout: skin

skin_name: "magenta Cyan Minimal"
forum_thread_id: 849183
date_added: 2021-06-24
game_modes:
    - standard
    - mania
author:
    - 2318309
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "magenta Cyan Minimal"
      screenshots:
          - "http://i.imgur.com/e6f6Kzg.png"
          - "http://i.imgur.com/CysIeIX.png"
          - "http://i.imgur.com/m6Ims8v.png"
          - "http://i.imgur.com/yEmszK0.png"
          - "http://i.imgur.com/ElLxLoi.png"
          - "http://i.imgur.com/UVQVjYv.png"
          - "http://i.imgur.com/cZBQyGz.png"
          - "http://i.imgur.com/KTmotxD.png"
          - "http://i.imgur.com/Dl0qEXJ.png"
videos:
    skinship:
    author:
---
