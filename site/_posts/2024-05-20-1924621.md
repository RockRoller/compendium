---
layout: skin

skin_name: "Sparkle"
forum_thread_id: 1924621
date_added: 2025-01-25
game_modes:
    - standard
    - catch
author:
    - 10081404
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "honkai_star_rail"
    - "honkai_impact_3rd"
categories:
    - anime
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Sparkle"
      screenshots:
          - "https://i.imgur.com/hDL3oMD.png"
          - "https://i.imgur.com/t35ETU8.png"
          - "https://i.imgur.com/CHk4bAp.png"
          - "https://i.imgur.com/6udDUxg.png"
          - "https://i.imgur.com/LRcWgBe.png"
          - "https://i.imgur.com/c2xDahI.png"
          - "https://i.imgur.com/bwN9l7M.png"
          - "https://i.imgur.com/NaicaVJ.png"
          - "https://i.imgur.com/0oZgwHA.png"
          - "https://i.imgur.com/73SEqNn.png"
          - "https://i.imgur.com/ghl2EPr.png"
          - "https://i.imgur.com/PaT0M5y.png"
          - "https://i.imgur.com/APegnMl.png"
videos:
    skinship:
    author:
---
