---
layout: skin

skin_name: "Sakurada Shiro"
forum_thread_id: 1605262
date_added: 2023-02-07
game_modes:
    - standard
author:
    - 15365050
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "sakurada_shiro"
    - "hy_plus"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Sakurada Shiro"
      screenshots:
          - "https://i.imgur.com/B6B1oxv.png"
          - "https://i.imgur.com/g6tT3dt.png"
          - "https://i.imgur.com/KERlp1X.png"
          - "https://i.imgur.com/eZhcRaj.png"
          - "https://i.imgur.com/Kq3OtOi.png"
          - "https://i.imgur.com/hBqTUGh.png"
          - "https://i.imgur.com/3Juq54V.png"
          - "https://i.imgur.com/tN6XWfM.png"
          - "https://i.imgur.com/5SL9tmc.png"
          - "https://i.imgur.com/UKsZOzw.png"
          - "https://i.imgur.com/hTwbWwX.png"
          - "https://i.imgur.com/fzLV5aJ.png"
          - "https://i.imgur.com/4AZCd00.png"
videos:
    skinship:
    author:
---
