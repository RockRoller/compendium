---
layout: skin

skin_name: "Nanamori"
forum_thread_id: 948438
date_added: 2021-06-28
game_modes:
    - standard
    - mania
    - catch
author:
    - 3060807
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "yuru_yuri"
categories:
    - anime
skin_collection:
    - name: "Nanamori"
      screenshots:
          - "https://i.imgur.com/Y5XMBGd.png"
          - "https://i.imgur.com/8S46VUj.png"
          - "https://i.imgur.com/VMzSBQJ.png"
          - "https://i.imgur.com/LUX3dhG.png"
          - "https://i.imgur.com/RklZOMI.png"
          - "https://i.imgur.com/5gNWpyO.png"
          - "https://i.imgur.com/j2A6cIo.jpg"
          - "https://i.imgur.com/N1bFkO4.png"
          - "https://i.imgur.com/FnVHujx.png"
          - "https://i.imgur.com/PK6T2G8.png"
          - "https://i.imgur.com/tp8MSta.png"
          - "https://i.imgur.com/WAtQvkX.png"
          - "https://i.imgur.com/MIAli9k.png"
          - "https://i.imgur.com/F2aoHCL.png"
          - "https://i.imgur.com/iTvxLpv.png"
          - "https://i.imgur.com/zNtrIDl.png"
videos:
    skinship:
    author:
---
