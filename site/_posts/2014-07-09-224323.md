---
layout: skin

skin_name: "東方Project - Hata no Kokoro"
forum_thread_id: 224323
date_added: 2021-06-28
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 3717733
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hata_no_kokoro"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "東方Project - Hata no Kokoro"
      screenshots:
          - "https://i.imgur.com/Z3E9cCC.png"
          - "https://i.imgur.com/ZKxA4sI.png"
          - "https://i.imgur.com/yGwic0H.png"
          - "https://i.imgur.com/rCML0Yg.png"
          - "https://i.imgur.com/09rvZbp.png"
          - "https://i.imgur.com/89QOg7l.png"
          - "https://i.imgur.com/nwlBhV1.png"
          - "https://i.imgur.com/4m1v8lw.png"
          - "https://i.imgur.com/NszYyHi.png"
          - "https://i.imgur.com/X910i7L.png"
          - "https://i.imgur.com/FBQAkEp.png"
          - "https://i.imgur.com/FYaILxW.png"
          - "https://i.imgur.com/QOohtzh.png"
          - "https://i.imgur.com/7dbthon.png"
videos:
    skinship:
    author:
---
