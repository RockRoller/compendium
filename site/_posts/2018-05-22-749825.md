---
layout: skin

skin_name: "kaimu"
forum_thread_id: 749825
date_added: 2021-07-18
game_modes:
    - standard
author:
    - 6568505
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "kaimu"
      screenshots:
          - "http://i.imgur.com/zwJL1Bm.png"
          - "http://i.imgur.com/lHqead0.png"
          - "http://i.imgur.com/zBmhuMS.png"
          - "http://i.imgur.com/AXjf89f.png"
          - "http://i.imgur.com/WOeYpRx.png"
          - "http://i.imgur.com/rDSVdAI.png"
          - "http://i.imgur.com/q892CqJ.png"
          - "http://i.imgur.com/mUuF3aO.png"
          - "http://i.imgur.com/0kW4gaU.png"
          - "http://i.imgur.com/XqKg6xP.png"
          - "http://i.imgur.com/U0jAKg5.png"
videos:
    skinship:
    author:
---
