---
layout: skin

skin_name: "Musim Dingin"
forum_thread_id: 1480386
date_added: 2022-01-13
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_2_submission"
    - "cookie"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "winter"
categories:
    - minimalistic
skin_collection:
    - name: "Musim Dingin"
      screenshots:
          - "https://i.imgur.com/5qig8RX.jpeg"
          - "https://i.imgur.com/FijvyjJ.jpeg"
          - "https://i.imgur.com/e5P3v1p.jpeg"
          - "https://i.imgur.com/UpoHpAu.jpeg"
          - "https://i.imgur.com/DAXuQk4.jpeg"
          - "https://i.imgur.com/tQmcT3G.jpeg"
          - "https://i.imgur.com/55el1xb.jpeg"
          - "https://i.imgur.com/YGo33Xq.jpeg"
          - "https://i.imgur.com/3lBh9MO.jpeg"
          - "https://i.imgur.com/ToA6tFj.jpeg"
          - "https://i.imgur.com/mxX6oPw.jpeg"
          - "https://i.imgur.com/J7iucSa.jpeg"
          - "https://i.imgur.com/5MOV1zb.jpeg"
          - "https://i.imgur.com/Ise4iDx.jpeg"
          - "https://i.imgur.com/SvlPJwb.jpeg"
          - "https://i.imgur.com/5aGtKFW.jpeg"
          - "https://i.imgur.com/pbmPVFr.jpeg"
videos:
    skinship:
        - uISFsTYvz7g
    author:
---
