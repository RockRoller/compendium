---
layout: skin

skin_name: "eclipse"
forum_thread_id: 822535
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 9668736
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "eclipse"
      screenshots:
          - "http://i.imgur.com/NfWLO7X.jpg"
          - "http://i.imgur.com/bO6RFq3.jpg"
          - "http://i.imgur.com/fraktgd.jpg"
          - "http://i.imgur.com/phir6R8.jpg"
          - "http://i.imgur.com/v4FAgBA.png"
videos:
    skinship:
    author:
---
