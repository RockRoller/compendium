---
layout: skin

skin_name: "Usada Pekora Test"
forum_thread_id: 1473126
date_added: 2021-12-08
game_modes:
    - standard
author:
    - 3717733
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "vtuber"
    - "usada_pekora"
    - "hololive_3rd_generation"
categories:
    - anime
skin_collection:
    - name: "Usada Pekora Test"
      screenshots:
          - "https://i.imgur.com/tCxeVzn.png"
          - "https://i.imgur.com/3b0DCsK.jpg"
          - "https://i.imgur.com/Rkarg01.png"
          - "https://i.imgur.com/GcXY1oz.png"
          - "https://i.imgur.com/55cAiCN.png"
          - "https://i.imgur.com/kiEGxKR.png"
          - "https://i.imgur.com/6kYJnep.png"
          - "https://i.imgur.com/fr9e0AG.png"
          - "https://i.imgur.com/wJFiKsX.png"
          - "https://i.imgur.com/62CaBDl.png"
          - "https://i.imgur.com/Bd4L8g6.png"
          - "https://i.imgur.com/Yy4Ar7x.png"
          - "https://i.imgur.com/4wR9jMo.png"
          - "https://i.imgur.com/BGbh7J5.png"
videos:
    skinship:
    author:
---
