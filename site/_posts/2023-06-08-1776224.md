---
layout: skin

skin_name: "Greylooks"
forum_thread_id: 1776224
date_added: 2024-01-09
game_modes:
    - standard
    - taiko
    - catch
author:
    - 22283039
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Greylooks"
      screenshots:
          - "https://i.imgur.com/LvXr1RV.png"
          - "https://i.imgur.com/nIOInm7.png"
          - "https://i.imgur.com/tPnI0LG.png"
          - "https://i.imgur.com/bC6kvr4.png"
          - "https://i.imgur.com/fUdVOwD.png"
          - "https://i.imgur.com/SmwD13M.png"
          - "https://i.imgur.com/IjEtNuR.png"
          - "https://i.imgur.com/OOdKMPN.png"
          - "https://i.imgur.com/TUqpKnk.png"
          - "https://i.imgur.com/SMVOud8.png"
          - "https://i.imgur.com/4XdXmU4.png"
          - "https://i.imgur.com/zP2UDjR.png"
videos:
    skinship:
    author:
---
