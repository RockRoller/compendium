---
layout: skin

skin_name: "Ralsei Dark"
forum_thread_id: 1629393
date_added: 2022-09-06
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 15646039
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
tags:
    - "contest_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "contest_3_submission"
    - "deltarune"
    - "ralsei"
    - "tritanopia"
    - "contest_3_2nd_place"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Ralsei"
      screenshots:
          - "https://i.imgur.com/UP7eey8.jpg"
          - "https://i.imgur.com/jQDV9TV.jpg"
          - "https://i.imgur.com/q0lfnTG.jpg"
          - "https://i.imgur.com/yhdelKB.jpg"
          - "https://i.imgur.com/rxh8G12.jpg"
          - "https://i.imgur.com/MWZwkAt.jpg"
          - "https://i.imgur.com/VhApIBa.jpg"
          - "https://i.imgur.com/vJIzLAU.jpg"
          - "https://i.imgur.com/wxykxA0.jpg"
          - "https://i.imgur.com/iPFAdXE.jpg"
          - "https://i.imgur.com/GfEvfur.jpg"
          - "https://i.imgur.com/W965Nyp.jpg"
          - "https://i.imgur.com/HcTMro4.jpg"
          - "https://i.imgur.com/bVtCbIt.jpg"
          - "https://i.imgur.com/IBz9Njv.jpg"
          - "https://i.imgur.com/zf7mgXC.jpg"
          - "https://i.imgur.com/9H1Kq1h.jpg"
          - "https://i.imgur.com/BgM2RJ0.jpg"
    - name: "No Character"
      screenshots:
          - "https://i.imgur.com/SdUVvMr.jpg"
          - "https://i.imgur.com/NTtt9a2.jpg"
          - "https://i.imgur.com/NGfTTb8.jpg"
          - "https://i.imgur.com/1UsedTP.jpg"
          - "https://i.imgur.com/RmYgc7Z.jpg"
          - "https://i.imgur.com/iwcTxAv.jpg"
          - "https://i.imgur.com/nPnunqH.jpg"
          - "https://i.imgur.com/NzxjeAj.jpg"
          - "https://i.imgur.com/p7AOWDV.jpg"
          - "https://i.imgur.com/JoTzELF.jpg"
          - "https://i.imgur.com/ZgohVLN.jpg"
          - "https://i.imgur.com/gjiXtsV.jpg"
          - "https://i.imgur.com/XBwBuFU.jpg"
          - "https://i.imgur.com/AMlXlQU.jpg"
          - "https://i.imgur.com/h29Dl2U.jpg"
          - "https://i.imgur.com/hRTi8cI.jpg"
          - "https://i.imgur.com/zP0JeJZ.jpg"
          - "https://i.imgur.com/WKzJsH6.jpg"
videos:
    skinship:
        - Y-x_M8Jnpt4
    author:
---
