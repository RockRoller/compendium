---
layout: skin

skin_name: "Aqours! Sunshine!"
forum_thread_id: 506247
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 2636217
resolutions:
    - hd
    - sd
ratios:
tags:
    - "love_live"
    - "aquors"
categories:
    - anime
skin_collection:
    - name: "Aqours! Sunshine!"
      screenshots:
          - "http://i.imgur.com/fCwYEIS.jpg"
          - "http://i.imgur.com/O5dSHh5.jpg"
          - "http://i.imgur.com/CEnEhvn.jpg"
          - "http://i.imgur.com/QhZ482h.jpg"
          - "http://i.imgur.com/u3V1698.jpg"
          - "http://i.imgur.com/HZWF4bd.jpg"
videos:
    skinship:
    author:
---
