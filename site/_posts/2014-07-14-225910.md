---
layout: skin

skin_name: "Science"
forum_thread_id: 225910
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 3066136
resolutions:
    - hd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Science"
      screenshots:
          - "http://i.imgur.com/raYD3vF.png"
          - "http://i.imgur.com/wAKpf1x.png"
          - "http://i.imgur.com/91y5Isb.png"
          - "http://i.imgur.com/harUV06.jpg"
          - "http://i.imgur.com/KEbAl0n.png"
          - "http://i.imgur.com/f4kQsdM.jpg"
videos:
    skinship:
    author:
---
