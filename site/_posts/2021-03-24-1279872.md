---
layout: skin

skin_name: "Savoy Wave"
forum_thread_id: 1279872
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 15599657
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Savoy Wave"
      screenshots:
          - "https://i.imgur.com/N0rDOFB.png"
          - "https://i.imgur.com/0ys2fBd.png"
          - "https://i.imgur.com/GeX5r2y.png"
          - "https://i.imgur.com/VE9NkB0.png"
          - "https://i.imgur.com/Rlz6yel.png"
          - "https://i.imgur.com/EFG5Pk3.png"
          - "https://i.imgur.com/yEq3pXh.png"
          - "https://i.imgur.com/EpAkswN.png"
videos:
    skinship:
    author:
---
