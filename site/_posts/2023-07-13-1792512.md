---
layout: skin

skin_name: "Granat (Gris II)"
forum_thread_id: 1792512
date_added: 2023-07-20
game_modes:
    - standard
    - catch
author:
    - 2168518
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "animated"
    - "beatmap"
    - "contest_4_submission"
    - "granat"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Granat (Gris II)"
      screenshots:
          - "https://i.imgur.com/YpZzlRa.jpg"
          - "https://i.imgur.com/6M6knfA.jpg"
          - "https://i.imgur.com/YhxFLXK.jpg"
          - "https://i.imgur.com/BDevbJM.jpg"
          - "https://i.imgur.com/6VNt194.jpg"
          - "https://i.imgur.com/GP3vVbW.jpg"
          - "https://i.imgur.com/zuyozKq.jpg"
          - "https://i.imgur.com/SXJujl0.jpg"
          - "https://i.imgur.com/Lvq5WKA.jpg"
          - "https://i.imgur.com/EdA8rPW.jpg"
          - "https://i.imgur.com/CW14Dg0.jpg"
          - "https://i.imgur.com/nzSqaIZ.jpg"
videos:
    skinship:
        - dqXHllWzVSc
    author:
        - yzD2b9tZqzw
---
