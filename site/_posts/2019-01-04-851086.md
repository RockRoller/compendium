---
layout: skin

skin_name: "Rimuru Tempest"
forum_thread_id: 851086
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 5971775
    - 6642526
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "rimuru_tempest"
    - "tensei_shitara_slime_datta_ken"
    - "that_time_i_got_reincarnated_as_a_slime"
    - "tensura"
categories:
    - anime
skin_collection:
    - name: "Rimuru Tempest"
      screenshots:
          - "https://i.imgur.com/aRx1rQ8.png"
          - "https://i.imgur.com/EREVChH.png"
          - "https://i.imgur.com/hN4wJTN.png"
          - "https://i.imgur.com/GROeN09.png"
          - "https://i.imgur.com/wX5wzjw.png"
          - "https://i.imgur.com/k2jC8Vq.png"
videos:
    skinship:
    author:
---
