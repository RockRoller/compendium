---
layout: skin

skin_name: "KMSkin"
forum_thread_id: 578017
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 8234966
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "KMSkin"
      screenshots:
          - "http://i.imgur.com/MbnN89b.jpg"
          - "http://i.imgur.com/oe0Cvyj.png"
          - "http://i.imgur.com/u0zZiDA.png"
          - "http://i.imgur.com/HVwSzY2.png"
          - "http://i.imgur.com/gCjJskD.png"
          - "http://i.imgur.com/nx0pg9X.png"
          - "http://i.imgur.com/Qn3ETfv.png"
videos:
    skinship:
    author:
---
