---
layout: skin

skin_name: "Tsukiyuki Miyako"
forum_thread_id: 1755114
date_added: 2023-07-11
game_modes:
    - standard
author:
    - 6785191
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "blue_archive"
    - "tsukiyuki_miyako"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "Tsukiyuki Miyako"
      screenshots:
          - "https://i.imgur.com/W8Ew2G7.png"
          - "https://i.imgur.com/iK0gv9r.png"
          - "https://i.imgur.com/IY1fM2n.png"
          - "https://i.imgur.com/evDgLp2.png"
          - "https://i.imgur.com/XMGc3yz.png"
          - "https://i.imgur.com/4U5QOPD.png"
          - "https://i.imgur.com/iOZNLf3.png"
          - "https://i.imgur.com/uqjDeil.png"
          - "https://i.imgur.com/6tM84qa.png"
          - "https://i.imgur.com/wr85rv8.png"
          - "https://i.imgur.com/0HI1hOs.png"
          - "https://i.imgur.com/DtGia4D.png"
videos:
    skinship:
    author:
---
