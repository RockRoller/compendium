---
layout: skin

skin_name: "Loona"
forum_thread_id: 1285299
date_added: 2021-07-15
game_modes:
    - standard
author:
    - 2168518
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "k-pop"
    - "contest_submission"
    - "contest_1_submission"
    - "loona"
categories:
    - eyecandy
skin_collection:
    - name: "Loona"
      screenshots:
          - "https://i.imgur.com/UbGKwfd.jpeg"
          - "https://i.imgur.com/ywMmv3p.jpeg"
          - "https://i.imgur.com/OHOYvdg.jpeg"
          - "https://i.imgur.com/ZI7ksuk.jpeg"
          - "https://i.imgur.com/n7RRTTI.jpeg"
          - "https://i.imgur.com/0LdWXec.jpeg"
          - "https://i.imgur.com/ZiqXYAd.jpeg"
          - "https://i.imgur.com/2S2HgD8.jpeg"
          - "https://i.imgur.com/Vh3cd9d.jpeg"
          - "https://i.imgur.com/ZeAubNB.jpeg"
          - "https://i.imgur.com/0luabVz.jpeg"
videos:
    skinship:
        - _dM1K9b3sD0
    author:
---
