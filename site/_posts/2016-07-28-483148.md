---
layout: skin

skin_name: "osu! Next"
forum_thread_id: 483148
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 5844251
resolutions:
    - hd
    - sd
ratios:
tags:
    - "lazer"
categories:
    - game
skin_collection:
    - name: "osu! Next"
      screenshots:
          - "http://i.imgur.com/66B36LE.jpg"
          - "http://i.imgur.com/vb5rtRO.jpg"
          - "http://i.imgur.com/N7rBJZx.png"
          - "http://i.imgur.com/GwWkJZy.png"
          - "http://i.imgur.com/Oc1xmBo.png"
          - "http://i.imgur.com/V1q1bga.png"
          - "http://i.imgur.com/Sadz0OC.png"
          - "http://i.imgur.com/5lE9vem.png"
          - "http://i.imgur.com/ceHpUw4.jpg"
          - "http://i.imgur.com/AGUJsgA.png"
          - "http://i.imgur.com/I2R2Ocb.png"
videos:
    skinship:
    author:
---
