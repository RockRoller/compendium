---
layout: skin

skin_name: "osu!Modern Skin"
forum_thread_id: 462493
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 5395136
resolutions:
ratios:
tags:
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "osu!Modern Skin"
      screenshots:
          - "https://i.imgur.com/fWmRQ5R.jpg"
          - "https://i.imgur.com/BxdN624.png"
          - "https://i.imgur.com/HwaNZr4.png"
          - "https://i.imgur.com/kKcS4z6.png"
          - "https://i.imgur.com/JfZHPkw.png"
          - "https://i.imgur.com/49LU1xG.png"
          - "https://i.imgur.com/V0xAk0X.png"
          - "https://i.imgur.com/fiU12ka.png"
          - "https://i.imgur.com/KBO9BU0.png"
          - "https://i.imgur.com/Vh9iByL.png"
          - "https://i.imgur.com/9zmtgec.png"
          - "https://i.imgur.com/Sb9Y37z.png"
          - "https://i.imgur.com/PaMJb88.png"
videos:
    skinship:
    author:
---
