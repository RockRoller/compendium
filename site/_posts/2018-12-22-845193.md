---
layout: skin

skin_name: "DDR"
forum_thread_id: 845193
date_added: 2021-06-24
game_modes:
    - mania
author:
    - 7629682
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "ddr"
    - "dance_dance_revolution"
    - "vsrg"
categories:
    - eyecandy
    - game
skin_collection:
    - name: "DDR"
      screenshots:
          - "https://i.imgur.com/kbtPiD0.png"
          - "https://i.imgur.com/qKGoFU2.png"
          - "https://i.imgur.com/13WJSlU.png"
          - "https://i.imgur.com/wiNzJom.png"
          - "https://i.imgur.com/C9wh97F.png"
videos:
    skinship:
    author:
---
