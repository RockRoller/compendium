---
layout: skin

skin_name: "How You Like That"
forum_thread_id: 1364646
date_added: 2021-07-15
game_modes:
    - standard
author:
    - 4236855
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "k-pop"
    - "blackpink"
    - "music"
categories:
    - minimalistic
skin_collection:
    - name: "How You Like That"
      screenshots:
          - "https://i.imgur.com/ThZa4l9.png"
          - "https://i.imgur.com/VLQqWUs.png"
          - "https://i.imgur.com/c20UHJR.png"
          - "https://i.imgur.com/i6jIIt2.png"
          - "https://i.imgur.com/5uRf5gq.png"
          - "https://i.imgur.com/jkDU5QS.png"
          - "https://i.imgur.com/f1E2QxT.jpeg"
          - "https://i.imgur.com/bQxxRub.jpeg"
          - "https://i.imgur.com/Uj9bcvk.jpeg"
          - "https://i.imgur.com/IvIeQ6p.jpeg"
          - "https://i.imgur.com/lHGxQJD.png"
videos:
    skinship:
    author:
---
