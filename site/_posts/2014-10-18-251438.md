---
layout: skin

skin_name: "Metalic"
forum_thread_id: 251438
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - catch
author:
    - 4320098
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Metalic"
      screenshots:
          - "http://i.imgur.com/kQ1BY70.png"
          - "http://i.imgur.com/MBjKn8S.png"
          - "http://i.imgur.com/BgTb0mY.png"
          - "http://i.imgur.com/ih8wf01.png"
videos:
    skinship:
    author:
---
