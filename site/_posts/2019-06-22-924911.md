---
layout: skin

skin_name: "Blue Sky"
forum_thread_id: 924911
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 13614274
resolutions:
    - sd
ratios:
    - "16:9"
tags:
categories:
    - other
skin_collection:
    - name: "Blue Sky"
      screenshots:
          - "https://imgur.com/V89TmML.jpg"
          - "https://imgur.com/gejBnUi.jpg"
          - "https://imgur.com/QzvQvBK.jpg"
          - "https://imgur.com/tt4h2yJ.jpg"
          - "https://imgur.com/9vQnhqq.jpg"
          - "https://imgur.com/ptJWnzJ.jpg"
          - "https://imgur.com/BfEf7ZW.jpg"
          - "https://imgur.com/jmF8aKP.jpg"
videos:
    skinship:
    author:
---
