---
layout: skin

skin_name: "Ayano Sugiura 2.0"
forum_thread_id: 1363154
date_added: 2021-07-15
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "yuru_yuri"
    - "ayano_sugiura"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Ayano Sugiura 2.0"
      screenshots:
          - "https://i.imgur.com/u89Hebq.png"
          - "https://i.imgur.com/NmGiNzh.png"
          - "https://i.imgur.com/MZF4xk9.png"
          - "https://i.imgur.com/aXfmwYM.png"
          - "https://i.imgur.com/H2QLkpZ.png"
          - "https://i.imgur.com/10LnVah.png"
          - "https://i.imgur.com/E9RkRtM.png"
videos:
    skinship:
    author:
---
