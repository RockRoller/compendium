---
layout: skin

skin_name: "StepOsu!Mania"
forum_thread_id: 512453
date_added: 2021-06-24
game_modes:
    - mania
author:
    - 8137541
resolutions:
ratios:
tags:
    - "stepmania"
    - "vsrg"
categories:
    - game
skin_collection:
    - name: "StepOsu!Mania"
      screenshots:
          - "http://i.imgur.com/Gw4Fuq5.png"
          - "http://i.imgur.com/Nrh1c9K.jpg"
          - "http://i.imgur.com/2mkyxLs.jpg"
          - "http://i.imgur.com/JIE6tAn.png"
          - "http://i.imgur.com/t8jCATF.png"
videos:
    skinship:
    author:
---
