---
layout: skin

skin_name: "Metal Gear osu!"
forum_thread_id: 194169
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2130664
resolutions:
    - hd
    - sd
ratios:
tags:
    - "metal_gear"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Metal Gear osu!"
      screenshots:
          - "http://i.imgur.com/jxqCM96.jpg"
          - "http://i.imgur.com/5y0mdHf.jpg"
          - "http://i.imgur.com/JcklUHG.png"
          - "http://i.imgur.com/s6niZv9.png"
          - "http://i.imgur.com/6Vvug6P.png"
videos:
    skinship:
    author:
---
