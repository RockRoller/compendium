---
layout: skin

skin_name: "Mountain 山"
forum_thread_id: 2022034
date_added: 2025-01-22
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 19135423
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "arknights"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "mountain"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Mountain 山"
      screenshots:
          - "https://i.imgur.com/6XUcx1b.png"
          - "https://i.imgur.com/m1qRCQ3.png"
          - "https://i.imgur.com/gedi9fx.png"
          - "https://i.imgur.com/qe6JFgh.png"
          - "https://i.imgur.com/KACUoCc.png"
          - "https://i.imgur.com/B85lHFr.png"
          - "https://i.imgur.com/4yWHKj1.png"
          - "https://i.imgur.com/8rYnKLq.png"
          - "https://i.imgur.com/3rNwqCO.png"
          - "https://i.imgur.com/Hk3LXFk.png"
videos:
    skinship:
    author:
---
