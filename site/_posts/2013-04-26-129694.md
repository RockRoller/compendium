---
layout: skin

skin_name: "o2jam"
forum_thread_id: 129694
date_added: 2021-06-24
game_modes:
    - mania
author:
    - 1984442
resolutions:
    - sd
ratios:
    - "all"
tags:
    - "o2jam"
    - "vsrg"
categories:
    - game
skin_collection:
    - name: "o2jam"
      screenshots:
          - "http://i.imgur.com/T6nLuI3.jpg"
    - name: "classic style"
      screenshots:
          - "http://i.imgur.com/tzYwULp.jpg"
    - name: "old version"
      screenshots:
          - "http://i.imgur.com/Txy92p1.jpg"
videos:
    skinship:
    author:
---
