---
layout: skin

skin_name: "quiltrue"
forum_thread_id: 1903814
date_added: 2024-04-14
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 502239
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "futuristic"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "arcade"
    - "bemani"
categories:
    - game
skin_collection:
    - name: "quiltrue"
      screenshots:
          - "https://i.imgur.com/sLWr1Um.png?2"
          - "https://i.imgur.com/xkh0pL9.png?2"
          - "https://i.imgur.com/xe1qee8.png?1"
          - "https://i.imgur.com/h6WjfRO.png?1"
          - "https://i.imgur.com/6tdlRuR.png"
          - "https://i.imgur.com/zG02g2Y.png"
          - "https://i.imgur.com/wBBtW8h.png"
          - "https://i.imgur.com/UvRaLtK.png"
          - "https://i.imgur.com/ZR3pcr7.png"
          - "https://i.imgur.com/HVrdG8I.png?1"
          - "https://i.imgur.com/wxpBTYs.png"
          - "https://i.imgur.com/ium9A5j.png"
          - "https://i.imgur.com/rKdo0Sb.png"
          - "https://i.imgur.com/lvjEksm.png"
          - "https://i.imgur.com/o1ExcyY.png"
          - "https://i.imgur.com/DbyNkFr.png"
          - "https://i.imgur.com/ni8kRjx.png"
          - "https://i.imgur.com/KG5Scfo.png?1"
          - "https://i.imgur.com/ZpAiFBD.png"
          - "https://i.imgur.com/YtU80Tq.png"
videos:
    skinship:
    author:
        - qmbrMkdZnLc
---
