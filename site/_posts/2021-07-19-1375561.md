---
layout: skin

skin_name: "Scuffed Osu"
forum_thread_id: 1375561
date_added: 2021-08-03
game_modes:
    - standard
author:
    - 21742832
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "paint"
    - "handdrawn"
categories:
    - joke
skin_collection:
    - name: "Scuffed Osu"
      screenshots:
          - "http://i.imgur.com/OotIyIa.jpg"
          - "http://i.imgur.com/PAyMy2s.jpg"
          - "http://i.imgur.com/g4cBUZG.jpg"
          - "http://i.imgur.com/htJOCug.jpg"
          - "http://i.imgur.com/pc7oBY8.jpg"
          - "http://i.imgur.com/4P54iZj.jpg"
videos:
    skinship:
    author:
---
