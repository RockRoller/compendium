---
layout: skin

skin_name: "If there was an endpoint."
forum_thread_id: 1672096
date_added: 2023-01-25
game_modes:
    - standard
author:
    - 19135423
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "tuyu"
    - "jpop"
    - "music"
    - "skin_of_the_year_2022_top_10"
    - "skin_of_the_year_2022_winner"
categories:
    - anime
    - minimalistic
    - eyecandy
skin_collection:
    - name: "If there was an endpoint."
      screenshots:
          - "https://i.imgur.com/HZKMX8K.png"
          - "https://i.imgur.com/omwr1Po.png"
          - "https://i.imgur.com/dFff6BR.png"
          - "https://i.imgur.com/bP17ks9.png"
          - "https://i.imgur.com/GcF1XW4.png"
          - "https://i.imgur.com/sdTmWAY.png"
          - "https://i.imgur.com/neAsI5b.png"
          - "https://i.imgur.com/NnccxH5.png"
videos:
    skinship:
        - Ew3gfYjavZc
    author:
---
