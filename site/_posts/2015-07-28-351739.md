---
layout: skin

skin_name: "Simplicity"
forum_thread_id: 351739
date_added: 2021-07-17
game_modes:
    - standard
author:
    - 1616175
resolutions:
    - hd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Simplicity"
      screenshots:
          - "http://i.imgur.com/9evNRr4.jpg"
          - "http://i.imgur.com/4ZrVeeW.jpg"
          - "http://i.imgur.com/1wmZCzy.jpg"
          - "http://i.imgur.com/ZyVDFPP.jpg"
          - "http://i.imgur.com/mn39U0a.jpg"
          - "http://i.imgur.com/iWqspCs.jpg"
videos:
    skinship:
    author:
---
