---
layout: skin

skin_name: "- 『Wintherest』 -"
forum_thread_id: 1498493
date_added: 2022-01-14
game_modes:
    - standard
    - catch
author:
    - 7122165
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_2_submission"
    - "winter"
    - "contest_2_winner"
    - "skin_of_the_year_2022_top_10"
categories:
    - minimalistic
skin_collection:
    - name: "- 『Wintherest』 -"
      screenshots:
          - "https://i.imgur.com/uw44glV.jpeg"
          - "https://i.imgur.com/xAbXNJA.jpeg"
          - "https://i.imgur.com/ijRjdCt.jpeg"
          - "https://i.imgur.com/iKcM0JR.jpeg"
          - "https://i.imgur.com/w5JoDBg.jpeg"
          - "https://i.imgur.com/sqL0z2O.jpeg"
          - "https://i.imgur.com/a4fJf0l.jpeg"
          - "https://i.imgur.com/uA1AMMe.jpeg"
          - "https://i.imgur.com/ejwAYdZ.jpeg"
          - "https://i.imgur.com/NYJY5TU.jpeg"
          - "https://i.imgur.com/FtJ53fU.jpeg"
          - "https://i.imgur.com/4q3x3qc.jpeg"
          - "https://i.imgur.com/gTQ29Z4.jpeg"
videos:
    skinship:
        - 8_UWmCKCTfc
    author:
---
