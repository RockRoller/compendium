---
layout: skin

skin_name: "BlackWhite"
forum_thread_id: 605064
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 10089574
resolutions:
    - hd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "BlackWhite"
      screenshots:
          - "https://i.imgur.com/sknzzHP.jpg"
          - "https://i.imgur.com/jdTo9kB.png"
          - "http://i.imgur.com/Qr8muWX.jpg"
          - "https://i.imgur.com/yBitFbp.png"
          - "https://i.imgur.com/SeMMT9C.png"
          - "https://i.imgur.com/O02tE8v.png"
          - "http://i.imgur.com/MuBRq6I.png"
          - "https://i.imgur.com/Vils3ue.png"
videos:
    skinship:
    author:
---
