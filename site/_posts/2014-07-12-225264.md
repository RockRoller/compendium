---
layout: skin

skin_name: "Chaosu-Futurity"
forum_thread_id: 225264
date_added: 2021-07-18
game_modes:
    - standard
    - catch
author:
    - 3621552
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Chaosu-Futurity"
      screenshots:
          - "http://i.imgur.com/YRcCxiv.jpg"
          - "http://i.imgur.com/OfDlGZ0.jpg"
          - "http://i.imgur.com/dzzRFpC.jpg"
          - "http://i.imgur.com/lkclifZ.jpg"
          - "http://i.imgur.com/Fxss48f.jpg"
          - "http://i.imgur.com/u3U6MZ1.jpg"
          - "http://i.imgur.com/XmfGt3W.jpg"
          - "http://i.imgur.com/gpzkgsF.jpg"
          - "http://i.imgur.com/45nHlC2.jpg"
          - "http://i.imgur.com/Q1Hwxe9.jpg"
          - "http://i.imgur.com/0H7w0bL.jpg"
          - "http://i.imgur.com/vQ2ncD8.jpg"
          - "http://i.imgur.com/gBDRbf3.jpg"
          - "http://i.imgur.com/7YQQeKH.jpg"
videos:
    skinship:
    author:
---
