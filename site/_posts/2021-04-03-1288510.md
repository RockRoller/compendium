---
layout: skin

skin_name: "Rhombus"
forum_thread_id: 1288510
date_added: 2021-07-15
game_modes:
    - standard
    - mania
author:
    - 14059135
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
categories:
    - minimalistic
skin_collection:
    - name: "Rhombus"
      screenshots:
          - "https://i.imgur.com/X19jTCQ.jpeg"
          - "https://i.imgur.com/ZsMEXqM.jpeg"
          - "https://i.imgur.com/1QazpMr.jpeg"
          - "https://i.imgur.com/W004fVV.jpeg"
          - "https://i.imgur.com/YTuxerq.jpeg"
          - "https://i.imgur.com/sbzyVMQ.jpeg"
          - "https://i.imgur.com/flf4xN0.jpeg"
          - "https://i.imgur.com/Z5gxG6E.jpeg"
          - "https://i.imgur.com/FP02vdZ.jpeg"
          - "https://i.imgur.com/22oG6k3.jpeg"
          - "https://i.imgur.com/IOvc53z.jpeg"
          - "https://i.imgur.com/xm3jeOW.jpeg"
          - "https://i.imgur.com/I15iRIR.jpeg"
          - "https://i.imgur.com/3X5pFXL.jpeg"
videos:
    skinship:
        - _M5yO2uWsR0
    author:
---
