---
layout: skin

skin_name: "Vladilena Milizé (Lena) 2.0"
forum_thread_id: 1698047
date_added: 2023-01-25
game_modes:
    - standard
author:
    - 21067731
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "86"
    - "vladilena_milizé"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Vladilena Milizé (Lena) 2.0"
      screenshots:
          - "https://i.imgur.com/5S3Ru1Z.png"
          - "https://i.imgur.com/zOvT6Lk.png"
          - "https://i.imgur.com/PP9tMLU.png"
          - "https://i.imgur.com/yraCe80.png"
          - "https://i.imgur.com/QRNBFvJ.png"
          - "https://i.imgur.com/1PT224d.png"
          - "https://i.imgur.com/Nopd3kY.png"
          - "https://i.imgur.com/lrXc6CK.png"
          - "https://i.imgur.com/xgaR0vi.png"
          - "https://i.imgur.com/ISEEnpX.png"
          - "https://i.imgur.com/6mr79dk.png"
          - "https://i.imgur.com/p2Tpcbe.png"
videos:
    skinship:
    author:
---
