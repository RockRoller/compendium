---
layout: skin

skin_name: "Superstar"
forum_thread_id: 1096841
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 11594132
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Superstar"
      screenshots:
          - "https://i.imgur.com/cGxqUo7.jpg"
          - "https://i.imgur.com/1k2sv02.png"
          - "https://i.imgur.com/RCAhBzp.png"
          - "https://i.imgur.com/JuGDmgv.png"
          - "https://i.imgur.com/VTC9l5X.png"
          - "https://i.imgur.com/6HGZKuN.png"
          - "https://i.imgur.com/s2DJ7ub.png"
          - "https://i.imgur.com/oCzq3sG.png"
          - "https://i.imgur.com/0LqMrD5.png"
videos:
    skinship:
    author:
---
