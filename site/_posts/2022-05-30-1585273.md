---
layout: skin

skin_name: "「PGR」Karenina Ember"
forum_thread_id: 1585273
date_added: 2022-06-22
game_modes:
    - standard
author:
    - 16086912
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "karenina"
    - "ember"
    - "punishing:_gray_raven"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "「PGR」Karenina Ember"
      screenshots:
          - "https://i.imgur.com/V58RenP.png"
          - "https://i.imgur.com/rsFJOCE.png"
          - "https://i.imgur.com/7NNZUy1.png"
          - "https://i.imgur.com/bCfdy6p.png"
          - "https://i.imgur.com/Lai6Bvt.png"
          - "https://i.imgur.com/Ut3MTV8.png"
          - "https://i.imgur.com/p34J1Pl.png"
          - "https://i.imgur.com/9gY7V6h.png"
          - "https://i.imgur.com/3dlvNrz.png"
          - "https://i.imgur.com/O0adr6R.png"
videos:
    skinship:
    author:
---
