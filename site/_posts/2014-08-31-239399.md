---
layout: skin

skin_name: "IA - Aria on the Planetes"
forum_thread_id: 239399
date_added: 2021-06-28
game_modes:
    - standard
    - catch
author:
    - 3717733
resolutions:
ratios:
tags:
    - "ia"
    - "vocaloid"
categories:
    - anime
skin_collection:
    - name: "IA - Aria on the Planetes"
      screenshots:
          - "http://i.imgur.com/sZqjRGT.jpg"
          - "http://i.imgur.com/1rR8jnX.jpg"
          - "http://i.imgur.com/o8UBTqi.png"
          - "http://i.imgur.com/33QK0it.jpg"
          - "http://i.imgur.com/LMtEpBL.jpg"
          - "http://i.imgur.com/BZ9s3Sw.png"
videos:
    skinship:
    author:
---
