---
layout: skin

skin_name: "Project Amber"
forum_thread_id: 1471014
date_added: 2022-02-03
game_modes:
    - standard
author:
    - 12221087
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
tags:
    - "genshin_impact"
    - "amber"
categories:
    - anime
    - game
skin_collection:
    - name: "Project Amber"
      screenshots:
          - "https://i.imgur.com/ML12Jr8.jpeg"
          - "https://i.imgur.com/pL86U0M.jpeg"
          - "https://i.imgur.com/Q6Sii6l.jpeg"
          - "https://i.imgur.com/pcyR6dG.jpeg"
          - "https://i.imgur.com/71u3nGE.jpeg"
          - "https://i.imgur.com/iRFhiVg.jpeg"
          - "https://i.imgur.com/EGB3au8.jpeg"
          - "https://i.imgur.com/j6TXkki.jpeg"
          - "https://i.imgur.com/p1YwHUo.jpeg"
          - "https://i.imgur.com/fjQC72h.jpeg"
    - name: "DT Gameplay"
      screenshots:
          - "https://i.imgur.com/aO1ohCe.jpeg"
    - name: "SZ Gameplay"
      screenshots:
          - "https://i.imgur.com/xIYItVD.jpeg"
videos:
    skinship:
    author:
---
