---
layout: skin

skin_name: "Yuru Camp"
forum_thread_id: 719356
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 9039378
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "yuru_camp"
    - "laid-back_camp"
    - "yurukyan"
categories:
    - anime
skin_collection:
    - name: "Yuru Camp"
      screenshots:
          - "https://i.imgur.com/zKR4TnU.png"
          - "https://i.imgur.com/vUJ7cGS.png"
          - "https://i.imgur.com/LefoUW1.png"
          - "https://i.imgur.com/CGeWT5y.png"
          - "https://i.imgur.com/WhsqlwS.png"
          - "https://i.imgur.com/IzAAuaA.png"
          - "https://i.imgur.com/WKx22TJ.png"
          - "https://i.imgur.com/RykflzT.png"
          - "https://i.imgur.com/EJFSlEn.png"
          - "https://i.imgur.com/U2EVu7S.png"
          - "https://i.imgur.com/VTMiIoA.png"
          - "https://i.imgur.com/VQDpBlA.png"
          - "https://i.imgur.com/FbCWOMc.png"
          - "https://i.imgur.com/xoVio39.png"
          - "https://i.imgur.com/4Cfw7ie.png"
          - "https://i.imgur.com/akaU793.png"
          - "https://i.imgur.com/DMprTaV.png"
          - "https://i.imgur.com/TvGBfvi.png"
          - "https://i.imgur.com/6fWKse6.png"
          - "https://i.imgur.com/v6CsKEg.png"
          - "https://i.imgur.com/0o0ljBV.png"
          - "https://i.imgur.com/w9iSnlg.png"
videos:
    skinship:
    author:
---
