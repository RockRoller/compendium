---
layout: skin

skin_name: "wotoha"
forum_thread_id: 1827084
date_added: 2023-12-17
game_modes:
    - standard
author:
    - 6785191
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "wotoha"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "wotoha"
      screenshots:
          - "https://i.imgur.com/Uyqpa0L.png"
          - "https://i.imgur.com/JkmreIq.png"
          - "https://i.imgur.com/K4xlsSs.png"
          - "https://i.imgur.com/aWtUTGj.png"
          - "https://i.imgur.com/UwCLibO.png"
          - "https://i.imgur.com/BLYdRzX.png"
          - "https://i.imgur.com/OyBgPu4.png"
          - "https://i.imgur.com/opJvlQS.png"
          - "https://i.imgur.com/jpr388A.png"
          - "https://i.imgur.com/QCy8A64.png"
          - "https://i.imgur.com/vUDiSJ2.png"
          - "https://i.imgur.com/qdOCskd.png"
          - "https://i.imgur.com/TSHz9U5.png"
          - "https://i.imgur.com/wFHGoCz.png"
videos:
    skinship:
    author:
---
