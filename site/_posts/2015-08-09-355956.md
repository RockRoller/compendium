---
layout: skin

skin_name: "Flat Surroundings"
forum_thread_id: 355956
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 3252321
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Flat Surroundings"
      screenshots:
          - "http://i.imgur.com/vFPV3Gb.jpg"
          - "http://i.imgur.com/x42vZO3.jpg"
          - "http://i.imgur.com/R8uoWx0.jpg"
          - "http://i.imgur.com/PUkj9Nb.jpg"
          - "http://i.imgur.com/a4225rE.jpg"
          - "http://i.imgur.com/DLIvAX8.jpg"
          - "http://i.imgur.com/8EZDN5m.jpg"
          - "http://i.imgur.com/8E6dJRw.jpg"
videos:
    skinship:
    author:
---
