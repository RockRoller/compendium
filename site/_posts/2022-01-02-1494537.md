---
layout: skin

skin_name: "The Lost Winter"
forum_thread_id: 1494537
date_added: 2022-01-14
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 11844975
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_2_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "winter"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - other
skin_collection:
    - name: "The Lost Winter"
      screenshots:
          - "https://i.imgur.com/oEQItxf.jpeg"
          - "https://i.imgur.com/sIQoFkb.jpeg"
          - "https://i.imgur.com/aDDbcC6.jpeg"
          - "https://i.imgur.com/AJe1aot.jpeg"
          - "https://i.imgur.com/7XRUR1k.jpeg"
          - "https://i.imgur.com/WHrtInq.jpeg"
          - "https://i.imgur.com/saSrs3E.jpeg"
          - "https://i.imgur.com/rsUyv1q.jpeg"
          - "https://i.imgur.com/KleEaZj.jpeg"
          - "https://i.imgur.com/d1sVLcU.jpeg"
          - "https://i.imgur.com/MNuNsnM.jpeg"
          - "https://i.imgur.com/jXOlK8E.jpeg"
          - "https://i.imgur.com/bqRMt3g.jpeg"
          - "https://i.imgur.com/lQjwDRc.jpeg"
          - "https://i.imgur.com/fe8Xlwf.jpeg"
          - "https://i.imgur.com/Z4VREE0.jpeg"
          - "https://i.imgur.com/id70lcz.jpeg"
videos:
    skinship:
        - IHPwjBp6CKE
    author:
---
