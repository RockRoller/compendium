---
layout: skin

skin_name: "schizophrenic"
forum_thread_id: 1853052
date_added: 2023-12-23
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7082132
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "schizophrenic"
    - "mental_disorder"
categories:
    - minimalistic
skin_collection:
    - name: "schizophrenic"
      screenshots:
          - "https://i.imgur.com/SbNi7VC.jpg"
          - "https://i.imgur.com/PtRAOh8.jpg"
          - "https://i.imgur.com/xXfE2cG.jpg"
          - "https://i.imgur.com/h0vNS7O.jpg"
          - "https://i.imgur.com/HAJyG2a.jpg"
          - "https://i.imgur.com/1YTNrYJ.jpg"
          - "https://i.imgur.com/DzVoTY9.jpg"
          - "https://i.imgur.com/ddVUq2b.jpg"
          - "https://i.imgur.com/wPPU9tH.jpg"
          - "https://i.imgur.com/Hy6jggP.jpg"
          - "https://i.imgur.com/UKlPmUn.jpg"
          - "https://i.imgur.com/CujxjMO.jpg"
          - "https://i.imgur.com/7E2RQcc.jpg"
          - "https://i.imgur.com/dBRnWP7.jpg"
          - "https://i.imgur.com/XrB73ow.jpg"
          - "https://i.imgur.com/b2aAMG6.jpg"
          - "https://i.imgur.com/IDE5P58.jpg"
          - "https://i.imgur.com/549szFh.jpg"
          - "https://i.imgur.com/cKXMPJB.jpg"
          - "https://i.imgur.com/THIyKmR.jpg"
          - "https://i.imgur.com/7xDZP9D.jpg"
videos:
    skinship:
    author:
---
