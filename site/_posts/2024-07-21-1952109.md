---
layout: skin

skin_name: "Hakurei Reimu"
forum_thread_id: 1952109
date_added: 2024-07-22
game_modes:
    - standard
author:
    - 27835528
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "reimu_hakurei"
    - "touhou_project"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Hakurei Reimu"
      screenshots:
          - "https://i.imgur.com/EvhoxMR.png?1"
          - "https://i.imgur.com/I2U9Ra6.png?1"
          - "https://i.imgur.com/836a0pX.png?2"
          - "https://i.imgur.com/26QCskN.png?2"
          - "https://i.imgur.com/ZsjjJW3.png?1"
          - "https://i.imgur.com/aZlVOQZ.png?1"
          - "https://i.imgur.com/ZVXcHVw.png?1"
          - "https://i.imgur.com/2Jvotsr.png?1"
          - "https://i.imgur.com/mZm8pRM.png?2"
          - "https://i.imgur.com/5RKJiPT.png?1"
          - "https://i.imgur.com/q6aBvS9.jpg"
          - "https://i.imgur.com/nzB7j7A.jpg"
videos:
    skinship:
    author:
---
