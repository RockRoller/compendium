---
layout: skin

skin_name: "Forsaken Sakura"
forum_thread_id: 893794
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 11880689
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "cherry_blossoms"
    - "sakura"
categories:
    - other
skin_collection:
    - name: "Forsaken Sakura"
      screenshots:
          - "http://i.imgur.com/IZODTgs.jpg"
          - "http://i.imgur.com/bn6aXD0.jpg"
          - "http://i.imgur.com/VIvXCrj.jpg"
          - "http://i.imgur.com/sVPjEwt.jpg"
          - "http://i.imgur.com/apKgxWJ.jpg"
          - "http://i.imgur.com/q16k4th.jpg"
          - "http://i.imgur.com/RfH6oB3.jpg"
          - "http://i.imgur.com/0TUL6SQ.jpg"
          - "http://i.imgur.com/rYmBfkw.jpg"
          - "http://i.imgur.com/FgCQnKr.jpg"
          - "http://i.imgur.com/2FSZgkO.jpg"
          - "http://i.imgur.com/7whCLn8.jpg"
          - "http://i.imgur.com/4LauNRd.jpg"
          - "http://i.imgur.com/07TGTJU.jpg"
          - "http://i.imgur.com/Vx352k3.jpg"
          - "http://i.imgur.com/ml4mYkq.jpg"
videos:
    skinship:
    author:
---
