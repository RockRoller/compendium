---
layout: skin

skin_name: "Azure"
forum_thread_id: 1929465
date_added: 2024-06-05
game_modes:
    - standard
author:
    - 8132473
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hatsune_miku"
    - "vocaloid"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Azure"
      screenshots:
          - "https://i.imgur.com/Mnw8x2J.jpg"
          - "https://i.imgur.com/Sz9cAF9.jpg"
          - "https://i.imgur.com/E77Vvb9.jpg"
          - "https://i.imgur.com/uJCqjyU.jpg"
          - "https://i.imgur.com/GhCMdGH.jpg"
          - "https://i.imgur.com/up6LfMd.jpg"
          - "https://i.imgur.com/2e32zI0.jpg"
          - "https://i.imgur.com/yS6oZ7o.jpg"
          - "https://i.imgur.com/yCNMouq.jpg"
          - "https://i.imgur.com/8xkWcEh.jpg"
          - "https://i.imgur.com/2uZfDyy.jpg"
          - "https://i.imgur.com/TlJv3AT.jpg"
          - "https://i.imgur.com/MbQK9Bi.jpg"
          - "https://i.imgur.com/gFs2V5X.jpg"
videos:
    skinship:
    author:
---
