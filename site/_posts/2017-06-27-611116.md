---
layout: skin

skin_name: "Busy's Skins"
forum_thread_id: 611116
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 4410656
resolutions:
ratios:
tags:
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Busy's Skins"
      screenshots:
          - "http://i.imgur.com/345CEL1.png"
          - "http://i.imgur.com/zAjZYbC.png"
          - "http://i.imgur.com/61dnhiS.png"
          - "http://i.imgur.com/BFCRyfB.png"
          - "http://i.imgur.com/dQ9uh4A.png"
          - "http://i.imgur.com/3Ik461t.png"
          - "http://i.imgur.com/3J07Krh.png"
          - "http://i.imgur.com/8QQRinW.png"
          - "http://i.imgur.com/8cN78GG.png"
videos:
    skinship:
    author:
---
