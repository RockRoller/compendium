---
layout: skin

skin_name: "taikomania IIDX 13 DonderfuL"
forum_thread_id: 151549
date_added: 2021-06-24
game_modes:
    - taiko
author:
    - 9974
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "taikomania IIDX 13 DonderfuL"
      screenshots:
          - "http://i.imgur.com/8ddwewz.png"
          - "http://i.imgur.com/MDFg9GM.png"
videos:
    skinship:
    author:
---
