---
layout: skin

skin_name: "ROMB"
forum_thread_id: 932937
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 7629682
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "ROMB"
      screenshots:
          - "http://i.imgur.com/N4O5EjK.png"
          - "http://i.imgur.com/A1edh1g.png"
          - "http://i.imgur.com/HLENWcc.png"
          - "http://i.imgur.com/t4LoBmi.png"
          - "http://i.imgur.com/9THavXM.png"
          - "http://i.imgur.com/cwJVGqA.png"
videos:
    skinship:
    author:
---
