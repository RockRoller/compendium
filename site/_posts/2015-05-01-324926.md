---
layout: skin

skin_name: "Prism"
forum_thread_id: 324926
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 1863682
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Prism"
      screenshots:
          - "http://i.imgur.com/cElwGqd.jpg"
          - "http://i.imgur.com/91Mahmm.jpg"
          - "http://i.imgur.com/2agVY9l.png"
          - "http://i.imgur.com/UFR5Ivd.png"
          - "http://i.imgur.com/O24ckrN.png"
videos:
    skinship:
    author:
---
