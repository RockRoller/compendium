---
layout: skin

skin_name: "PurProject"
forum_thread_id: 391298
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
    - catch
author:
    - 2244606
resolutions:
ratios:
tags:
categories:
    - eyecandy
skin_collection:
    - name: "PurProject"
      screenshots:
          - "http://i.imgur.com/2RGo5oB.jpg"
          - "http://i.imgur.com/sC3q3VE.jpg"
          - "http://i.imgur.com/fcO9Kya.png"
          - "http://i.imgur.com/NA9oVBA.png"
          - "http://i.imgur.com/Uavx3l6.png"
videos:
    skinship:
    author:
---
