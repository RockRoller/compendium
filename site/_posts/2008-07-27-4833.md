---
layout: skin

skin_name: "Xi-Style"
forum_thread_id: 4833
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 17894
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Xi-Style"
      screenshots:
          - "http://i.imgur.com/uSBhzQL.png"
          - "http://i.imgur.com/amWtsy6.jpg"
          - "http://i.imgur.com/R9mTT69.png"
          - "http://i.imgur.com/HTsisnW.png"
          - "http://i.imgur.com/5naVUWX.png"
videos:
    skinship:
    author:
---
