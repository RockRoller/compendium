---
layout: skin

skin_name: "AYAYA! AYAYA! Intensifies"
forum_thread_id: 842531
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 11188029
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "kiniro_mosaic"
    - "kinmoza"
    - "ayaya"
categories:
    - anime
skin_collection:
    - name: "AYAYA! AYAYA! Intensifies"
      screenshots:
          - "http://i.imgur.com/HoC1rY8.jpg"
          - "http://i.imgur.com/4u09OUA.jpg"
          - "http://i.imgur.com/QZEKNhE.jpg"
          - "http://i.imgur.com/xBvFci3.jpg"
          - "http://i.imgur.com/N5dwhvI.png"
          - "http://i.imgur.com/BH2eDto.png"
          - "http://i.imgur.com/xuzB9Bo.png"
          - "http://i.imgur.com/yeNKtkj.png"
videos:
    skinship:
    author:
---
