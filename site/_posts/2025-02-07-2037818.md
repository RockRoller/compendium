---
layout: skin

skin_name: "BLU25"
forum_thread_id: 2037818
date_added: 2025-02-07
game_modes:
    - standard
    - taiko
author:
    - 4236855
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "BLU25"
      screenshots:
          - "https://i.imgur.com/KLijGgv.jpeg"
          - "https://i.imgur.com/kZKPY2z.png"
          - "https://i.imgur.com/zl7vWEw.png"
          - "https://i.imgur.com/AebCMUJ.png"
          - "https://i.imgur.com/v1X6bKq.png"
          - "https://i.imgur.com/ZmA8zOH.png"
          - "https://i.imgur.com/ZstWlPm.png"
          - "https://i.imgur.com/mNyA3hF.png"
          - "https://i.imgur.com/JPH5y8W.png"
          - "https://i.imgur.com/wcOxvwy.png"
          - "https://i.imgur.com/XA9AnZj.png"
videos:
    skinship:
    author:
---
