---
layout: skin

skin_name: "movesense"
forum_thread_id: 1908139
date_added: 2025-01-24
game_modes:
    - standard
author:
    - 31294040
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "movesense"
      screenshots:
          - "https://i.imgur.com/NiHkrp3.png"
          - "https://i.imgur.com/gFOw9td.jpeg"
          - "https://i.imgur.com/oJLsDmO.jpeg"
          - "https://i.imgur.com/kWOY6lR.jpeg"
          - "https://i.imgur.com/AR8tSRR.jpeg"
videos:
    skinship:
    author:
---
