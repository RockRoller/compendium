---
layout: skin

skin_name: "To Love Ru"
forum_thread_id: 338041
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 4963834
resolutions:
    - hd
    - sd
ratios:
tags:
    - "to_love_ru"
categories:
    - anime
skin_collection:
    - name: "To Love Ru"
      screenshots:
          - "http://i.imgur.com/Tejc5mw.jpg"
          - "http://i.imgur.com/SNe50Ho.jpg"
          - "http://i.imgur.com/9Ppb4xA.png"
          - "http://i.imgur.com/iCnbsw4.jpg"
          - "http://i.imgur.com/ldYTd7E.png"
          - "http://i.imgur.com/vJLNYaB.jpg"
videos:
    skinship:
    author:
---
