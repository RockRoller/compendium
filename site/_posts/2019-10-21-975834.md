---
layout: skin

skin_name: "Nijigasaki High School Idol Club - Uehara Ayumu"
forum_thread_id: 975834
date_added: 2021-07-19
game_modes:
    - standard
author:
    - 11736686
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "a・zu・na"
    - "azuna"
    - "uehara_ayumu"
    - "love_live"
categories:
    - anime
skin_collection:
    - name: "Nijigasaki High School Idol Club - Uehara Ayumu"
      screenshots:
          - "http://tinyimg.io/i/pUCbTqi.png"
          - "http://tinyimg.io/i/vHbu1Vw.png"
          - "http://tinyimg.io/i/xw9pmF0.png"
          - "http://tinyimg.io/i/9TYiMj7.png"
          - "http://tinyimg.io/i/9ZeDNJQ.png"
          - "http://tinyimg.io/i/L7ctMZR.png"
          - "http://tinyimg.io/i/VVeUfEQ.png"
          - "http://tinyimg.io/i/6XDVynX.png"
videos:
    skinship:
    author:
---
