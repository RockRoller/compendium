---
layout: skin

skin_name: "吉田由紀 Yoshida Yuki"
forum_thread_id: 1546224
date_added: 2022-04-05
game_modes:
    - standard
author:
    - 4236855
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "given"
    - "yoshida_yuki"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "吉田由紀 Yoshida Yuki"
      screenshots:
          - "https://i.imgur.com/aqY8AMC.png"
          - "https://i.imgur.com/bKfH0xS.png"
          - "https://i.imgur.com/vYgdM15.png"
          - "https://i.imgur.com/JCidFzQ.png"
          - "https://i.imgur.com/ZDUnA0i.png"
          - "https://i.imgur.com/yRACIPr.png"
          - "https://i.imgur.com/xjLczs2.png"
          - "https://i.imgur.com/NLWE33k.png"
          - "https://i.imgur.com/SouZMBO.png"
videos:
    skinship:
    author:
---
