---
layout: skin

skin_name: "HiLyter/Highlighter"
forum_thread_id: 345720
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 4804628
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "HiLyter/Highlighter"
      screenshots:
          - "http://i.imgur.com/gJHhYjQ.png"
          - "http://i.imgur.com/U9ub7qx.png"
          - "http://i.imgur.com/mkgrY4o.png"
          - "http://i.imgur.com/CMN97qP.png"
videos:
    skinship:
    author:
---
