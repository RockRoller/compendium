---
layout: skin

skin_name: "Cherry Corolla ft. Mine-chan"
forum_thread_id: 1924546
date_added: 2024-05-20
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 5730417
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "mine-chan"
    - "masayo"
    - "gin_no_ame"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Cherry Corolla ft. Mine-chan"
      screenshots:
          - "https://i.imgur.com/KobbNQM.png"
          - "https://i.imgur.com/yOLKLdP.png"
          - "https://i.imgur.com/K3SnRCi.png"
          - "https://i.imgur.com/EjIMnVs.png"
          - "https://i.imgur.com/IcVMh0K.png"
          - "https://i.imgur.com/3SCEt5Q.png"
          - "https://i.imgur.com/R4M96Zr.png"
          - "https://i.imgur.com/VJLSyRt.png"
          - "https://i.imgur.com/Lv3U3d5.png"
          - "https://i.imgur.com/mHUHH70.png"
          - "https://i.imgur.com/kTZ3ix9.png"
          - "https://i.imgur.com/SKzgVYz.png"
          - "https://i.imgur.com/arLXtia.png"
          - "https://i.imgur.com/lgJGCtZ.png"
          - "https://i.imgur.com/2Md8SHR.png"
          - "https://i.imgur.com/RVZyyXD.png"
          - "https://i.imgur.com/QnXeLbx.png"
          - "https://i.imgur.com/qBwlc7x.png"
          - "https://i.imgur.com/pMObmaU.png"
          - "https://i.imgur.com/gMPwyPP.png"
videos:
    skinship:
    author:
---
