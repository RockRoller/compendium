---
layout: skin

skin_name: "Minimal"
forum_thread_id: 564208
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7971451
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Minimal"
      screenshots:
          - "https://i.imgur.com/wF3G4AJ.png"
          - "https://i.imgur.com/FGQDSWl.png"
          - "https://i.imgur.com/GeTgZkC.png"
          - "https://i.imgur.com/X3oeKSw.png"
          - "https://i.imgur.com/Fv3A62q.png"
          - "https://i.imgur.com/4hgt4LB.png"
          - "https://i.imgur.com/oe0VEcr.png"
          - "https://i.imgur.com/c7hs89N.png"
          - "https://i.imgur.com/BAEAuJn.png"
          - "https://i.imgur.com/5UeIy9n.png"
          - "https://i.imgur.com/uaEDExT.png"
          - "https://i.imgur.com/zHyH43X.png"
          - "https://i.imgur.com/7BJPNIN.png"
          - "https://i.imgur.com/jBSeYpP.png"
          - "https://i.imgur.com/88nFDMQ.png"
videos:
    skinship:
    author:
---
