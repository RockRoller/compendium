---
layout: skin

skin_name: "Project Miku"
forum_thread_id: 1583868
date_added: 2022-06-22
game_modes:
    - standard
author:
    - 17561095
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "glow"
    - "nakano_miku"
    - "5-toubun_no_hanayome"
    - "the_quintessential_quintuplets"
    - "gotoubun_no_hanayome"
categories:
    - anime
skin_collection:
    - name: "Project Miku"
      screenshots:
          - "https://i.imgur.com/qSerUKC.png"
          - "https://i.imgur.com/Mldt9kA.png"
          - "https://i.imgur.com/xZ5VAO5.png"
          - "https://i.imgur.com/CuUOMhY.png"
          - "https://i.imgur.com/gadz7bj.png"
          - "https://i.imgur.com/vhuzphN.png"
          - "https://i.imgur.com/4BXvCgP.png"
          - "https://i.imgur.com/KOUGyUG.png"
          - "https://i.imgur.com/qpGLyZH.png"
          - "https://i.imgur.com/i0zCS72.png"
          - "https://i.imgur.com/BRoCGev.png"
          - "https://i.imgur.com/XzpciSB.png"
          - "https://i.imgur.com/wfg0cdM.png"
          - "https://i.imgur.com/aFd1DLs.png"
          - "https://i.imgur.com/3ZdnxiZ.png"
          - "https://i.imgur.com/MTMUfDJ.png"
videos:
    skinship:
    author:
---
