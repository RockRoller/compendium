---
layout: skin

skin_name: "Cloudstepper"
forum_thread_id: 465824
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 616511
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Cloudstepper"
      screenshots:
          - "http://i.imgur.com/Zz0Uvlf.jpg"
          - "http://i.imgur.com/DsOzQO7.png"
          - "http://i.imgur.com/osQ1njn.png"
          - "http://i.imgur.com/f7mU0K5.jpg"
videos:
    skinship:
    author:
---
