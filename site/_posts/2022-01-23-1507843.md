---
layout: skin

skin_name: "> Crown"
forum_thread_id: 1507843
date_added: 2022-04-02
game_modes:
    - standard
author:
    - 13015586
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Light"
      screenshots:
          - "https://i.imgur.com/gqgSO90.png"
          - "https://i.imgur.com/PCvo3XK.png"
          - "https://i.imgur.com/cenrOel.png"
          - "https://i.imgur.com/z2msclC.png"
          - "https://i.imgur.com/ZcDVjW2.png"
          - "https://i.imgur.com/fitaoH7.png"
          - "https://i.imgur.com/wZ9m5KE.png"
          - "https://i.imgur.com/eDaWdyB.png"
          - "https://i.imgur.com/9I4174U.png"
          - "https://i.imgur.com/fNC69A3.png"
          - "https://i.imgur.com/KEPz1Ib.png"
    - name: "Dark"
      screenshots:
          - "https://i.imgur.com/w6PqoBL.png"
          - "https://i.imgur.com/BrS4hNo.png"
          - "https://i.imgur.com/pTE1UO5.png"
          - "https://i.imgur.com/z2msclC.png"
          - "https://i.imgur.com/ZklIW3t.png"
          - "https://i.imgur.com/fitaoH7.png"
          - "https://i.imgur.com/wfHqDRX.png"
          - "https://i.imgur.com/eDaWdyB.png"
          - "https://i.imgur.com/9I4174U.png"
          - "https://i.imgur.com/03ZiBmo.png"
          - "https://i.imgur.com/GcQakpK.png"
videos:
    skinship:
    author:
---
