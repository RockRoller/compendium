---
layout: skin

skin_name: "Northen Ocean Hime"
forum_thread_id: 319797
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 4085133
resolutions:
ratios:
tags:
    - "kantai_collection"
    - "northern_princess"
categories:
    - anime
skin_collection:
    - name: "Northen Ocean Hime"
      screenshots:
          - "http://i.imgur.com/z5gAEdw.jpg"
          - "http://i.imgur.com/CREa5rT.jpg"
          - "http://i.imgur.com/G0FWPvR.png"
          - "http://i.imgur.com/1xKMZVE.jpg"
          - "http://i.imgur.com/SBgSbh5.jpg"
          - "http://i.imgur.com/6Qnz96y.png"
videos:
    skinship:
    author:
---
