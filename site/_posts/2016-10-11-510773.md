---
layout: skin

skin_name: "Love Live! Halloween"
forum_thread_id: 510773
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2636217
resolutions:
    - hd
    - sd
ratios:
tags:
    - "love_live"
categories:
    - anime
skin_collection:
    - name: "Love Live! Halloween"
      screenshots:
          - "http://i.imgur.com/0jF78fM.jpg"
          - "http://i.imgur.com/iJrbTUZ.jpg"
          - "http://i.imgur.com/fG14udp.jpg"
          - "http://i.imgur.com/G0WLl8r.jpg"
          - "http://i.imgur.com/FkioeyR.jpg"
          - "http://i.imgur.com/aSpCh6t.jpg"
          - "http://i.imgur.com/5mfVZN8.jpg"
          - "http://i.imgur.com/7VFoGGc.jpg"
          - "http://i.imgur.com/mOQEsZo.jpg"
          - "http://i.imgur.com/laaUtQ1.jpg"
          - "http://i.imgur.com/PDjQIW8.jpg"
          - "http://i.imgur.com/WN1zSbt.jpg"
          - "http://i.imgur.com/9y0VVcN.jpg"
          - "http://i.imgur.com/e9z1ocH.jpg"
          - "http://i.imgur.com/xQJo6Oh.jpg"
          - "http://i.imgur.com/zumRGdR.jpg"
          - "http://i.imgur.com/VmACrey.jpg"
          - "http://i.imgur.com/7dRDyNP.jpg"
          - "http://i.imgur.com/A5MeV3L.jpg"
          - "http://i.imgur.com/2dSzloT.jpg"
          - "http://i.imgur.com/KcsPTI5.jpg"
videos:
    skinship:
    author:
---
