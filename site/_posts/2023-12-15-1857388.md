---
layout: skin

skin_name: "RESIDENT 7K with SUD+"
forum_thread_id: 1857388
date_added: 2024-01-09
game_modes:
    - mania
author:
    - 34529343
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "beatmania"
    - "7k"
categories:
    - game
skin_collection:
    - name: "RESIDENT 7K with SUD+"
      screenshots:
          - "https://i.imgur.com/7OddbyC.jpeg"
videos:
    skinship:
    author:
---
