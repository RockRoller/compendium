---
layout: skin

skin_name: "Lazuline Lutescent"
forum_thread_id: 1632482
date_added: 2022-09-06
game_modes:
    - standard
    - taiko
    - catch
author:
    - 15851364
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_3_submission"
    - "protanopia"
categories:
    - other
skin_collection:
    - name: "Lazuline Lutescent"
      screenshots:
          - "https://i.imgur.com/23RnfFR.png"
          - "https://i.imgur.com/HsA08sR.png"
          - "https://i.imgur.com/7k8EWti.png"
          - "https://i.imgur.com/keirFui.png"
          - "https://i.imgur.com/ehSj1r5.png"
          - "https://i.imgur.com/d9yodBb.png"
          - "https://i.imgur.com/SZhPqu1.png"
videos:
    skinship:
        - tohayWX-XiI
    author:
---
