---
layout: skin

skin_name: "Formal & Simple"
forum_thread_id: 87419
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 594863
resolutions:
    - hd
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Formal & Simple"
      screenshots:
          - "http://i.imgur.com/sLAO2u1.jpg"
          - "http://i.imgur.com/GLyBRO1.png"
          - "http://i.imgur.com/HHWSdsW.png"
          - "http://i.imgur.com/Fx0FfGH.png"
          - "http://i.imgur.com/3anqniw.png"
videos:
    skinship:
    author:
---
