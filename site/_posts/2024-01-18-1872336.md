---
layout: skin

skin_name: "Vain"
forum_thread_id: 1872336
date_added: 2024-02-04
game_modes:
    - standard
author:
    - 20564039
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Vain"
      screenshots:
          - "https://i.imgur.com/Yu1QVIw.png"
          - "https://i.imgur.com/Dd21ULW.png"
          - "https://i.imgur.com/NaJGtwD.png"
          - "https://i.imgur.com/YnuYwA3.png"
          - "https://i.imgur.com/Ym6j0jw.png"
          - "https://i.imgur.com/oL9Yglf.png"
          - "https://i.imgur.com/5ziwEuf.png"
          - "https://i.imgur.com/AS6qrfX.png"
          - "https://i.imgur.com/GaslBvU.png"
          - "https://i.imgur.com/y5sP5KG.png"
          - "https://i.imgur.com/lVABhAd.png"
videos:
    skinship:
    author:
---
