---
layout: skin

skin_name: "Baiken (Guilty Gear)"
forum_thread_id: 1608578
date_added: 2022-11-11
game_modes:
    - standard
author:
    - 12091015
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "animated"
    - "baiken"
    - "guilty_gear"
categories:
    - anime
    - game
skin_collection:
    - name: "Baiken (Guilty Gear)"
      screenshots:
          - "https://i.imgur.com/u5DTkC5.png"
          - "https://i.imgur.com/ItxJLub.png"
          - "https://i.imgur.com/G3gt0IC.png"
          - "https://i.imgur.com/W6fu9DG.png"
          - "https://i.imgur.com/6jKRZkv.png"
          - "https://i.imgur.com/HDIPuoD.png"
          - "https://i.imgur.com/kCHlzzm.png"
          - "https://i.imgur.com/reXRr08.png"
videos:
    skinship:
    author:
---
