---
layout: skin

skin_name: "IJN Yamashiro V2"
forum_thread_id: 1004844
date_added: 2021-07-19
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 12144912
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "yamashiro"
    - "azur_lane"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "IJN Yamashiro V2"
      screenshots:
          - "https://i.imgur.com/KNcor5b.png"
          - "https://i.imgur.com/aEpxIV2.png"
          - "https://i.imgur.com/Xg5phTi.png"
          - "https://i.imgur.com/LmlMeTI.png"
          - "https://i.imgur.com/j2xMcUE.png"
          - "https://i.imgur.com/jWd1l4r.png"
          - "https://i.imgur.com/4PMpokQ.png"
          - "https://i.imgur.com/n8qcHAQ.png"
          - "https://i.imgur.com/2D9gBZS.png"
          - "https://i.imgur.com/K6Uf0Fx.png"
          - "https://i.imgur.com/NaLipoF.png"
          - "https://i.imgur.com/BYCBHyX.png"
          - "https://i.imgur.com/6eifaqA.png"
          - "https://i.imgur.com/tf1ghFz.png"
          - "https://i.imgur.com/z1n6nQN.png"
          - "https://i.imgur.com/uAVKaUz.png"
          - "https://i.imgur.com/LlRkIKS.png"
          - "https://i.imgur.com/084AGSR.png"
          - "https://i.imgur.com/M02Nik2.png"
    - name: "non-weeb"
      screenshots:
videos:
    skinship:
    author:
---
