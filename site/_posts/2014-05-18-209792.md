---
layout: skin

skin_name: "Azure Fusion"
forum_thread_id: 209792
date_added: 2021-06-24
game_modes:
    - standard
    - mania
author:
    - 2130664
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Azure Fusion"
      screenshots:
          - "http://i.imgur.com/G6fmZKY.jpg"
          - "http://i.imgur.com/YtcsLGQ.png"
          - "http://i.imgur.com/NKixV32.png"
          - "http://i.imgur.com/zH94776.png"
          - "http://i.imgur.com/RS8rejw.png"
videos:
    skinship:
    author:
---
