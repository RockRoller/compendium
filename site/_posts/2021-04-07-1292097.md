---
layout: skin

skin_name: "Night05"
forum_thread_id: 1292097
date_added: 2021-07-15
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 15646039
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
    - "16:10"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "stars"
    - "nightsky"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - other
skin_collection:
    - name: "Night05"
      screenshots:
          - "https://i.imgur.com/QTTeKPa.jpeg"
          - "https://i.imgur.com/GXwk7Yd.jpeg"
          - "https://i.imgur.com/GkWoZZJ.jpeg"
          - "https://i.imgur.com/GWZ4jvd.jpeg"
          - "https://i.imgur.com/X1JMMjZ.jpeg"
          - "https://i.imgur.com/uPAYl8u.jpeg"
          - "https://i.imgur.com/IMqE0cx.jpeg"
          - "https://i.imgur.com/6FJxNjV.jpeg"
          - "https://i.imgur.com/pexAqgT.jpeg"
          - "https://i.imgur.com/wHQZcnd.jpeg"
          - "https://i.imgur.com/EDR67Mo.jpeg"
          - "https://i.imgur.com/cpgO3iI.jpeg"
          - "https://i.imgur.com/GAAnBTt.jpeg"
          - "https://i.imgur.com/fvY10wt.jpeg"
          - "https://i.imgur.com/VZuh9Tm.jpeg"
          - "https://i.imgur.com/5U1wyU7.jpeg"
          - "https://i.imgur.com/xfOMq6G.jpeg"
videos:
    skinship:
        - mA0hjLweDyw
    author:
---
