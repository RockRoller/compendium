---
layout: skin

skin_name: "Celestine Kaleidoscope"
forum_thread_id: 1871110
date_added: 2024-02-04
game_modes:
    - standard
author:
    - 19082107
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - eyecandy
skin_collection:
    - name: "Celestine Kaleidoscope"
      screenshots:
          - "https://i.imgur.com/VRCVQR6.png"
          - "https://i.imgur.com/tT2ZcDE.png"
          - "https://i.imgur.com/NGtIWd6.png"
          - "https://i.imgur.com/BDVkHUJ.png"
          - "https://i.imgur.com/bPpwCOJ.png"
          - "https://i.imgur.com/LZW34wy.png"
          - "https://i.imgur.com/id0jj4V.png"
videos:
    skinship:
    author:
---
