---
layout: skin

skin_name: "NeOsu"
forum_thread_id: 83087
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
    - catch
author:
    - 852867
resolutions:
ratios:
tags:
    - "neosu"
categories:
    - minimalistic
skin_collection:
    - name: "NeOsu"
      screenshots:
          - "http://i.imgur.com/BUGqmr1.jpg"
          - "http://i.imgur.com/KYIjZ5h.png"
          - "http://i.imgur.com/w1AvIiD.png"
          - "http://i.imgur.com/0urp9hK.png"
          - "http://i.imgur.com/ELyX3S0.png"
          - "http://i.imgur.com/uc91cUM.png"
          - "http://i.imgur.com/4Fq25hr.png"
videos:
    skinship:
    author:
---
