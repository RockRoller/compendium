---
layout: skin

skin_name: "Fairy Bread Bakeoff!! 2024"
forum_thread_id: 1974074
date_added: 2024-12-30
game_modes:
    - standard
author:
    - 11805037
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Fairy Bread Bakeoff!! 2024"
      screenshots:
          - "https://i.imgur.com/rZNTabM.jpg"
          - "https://i.imgur.com/YBtDCyt.jpg"
          - "https://i.imgur.com/fvMUmYk.jpg"
          - "https://i.imgur.com/EgV3Voa.jpg"
          - "https://i.imgur.com/msJSc3t.jpg"
          - "https://i.imgur.com/YLQGJuU.jpg"
          - "https://i.imgur.com/Mb8QUL4.jpg"
          - "https://i.imgur.com/BAnhBo3.jpg"
          - "https://i.imgur.com/JNICasP.jpg"
          - "https://i.imgur.com/Oq4LIzm.jpg"
          - "https://i.imgur.com/rinVouk.jpg"
videos:
    skinship:
    author:
---
