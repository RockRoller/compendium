---
layout: skin

skin_name: "kokopium"
forum_thread_id: 1728192
date_added: 2023-03-03
game_modes:
    - standard
author:
    - 6234482
    - 7122165
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "genshin_impact"
    - "skinners'_bulletin_showcase"
    - "sangonomiya_kokomi"
categories:
    - anime
    - minimalistic
    - eyecandy
    - game
skin_collection:
    - name: "kokopium"
      screenshots:
          - "https://i.imgur.com/1dO0pUn.png"
          - "https://i.imgur.com/O0luaj4.png"
          - "https://i.imgur.com/XqVpQ87.png"
          - "https://i.imgur.com/OfaJrfl.png"
          - "https://i.imgur.com/qP5ukre.png"
          - "https://i.imgur.com/aPcp0tP.png"
          - "https://i.imgur.com/JLM4g8H.png"
          - "https://i.imgur.com/XdKwWil.png"
          - "https://i.imgur.com/vcUjtmS.png"
          - "https://i.imgur.com/MZ4ra4s.png"
          - "https://i.imgur.com/G3FB18v.png"
videos:
    skinship:
        - Ai7Rz6EJrvw
    author:
---
