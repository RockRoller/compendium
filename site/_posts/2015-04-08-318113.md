---
layout: skin

skin_name: "The Everlasting Guilty Crown"
forum_thread_id: 318113
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 4427976
resolutions:
ratios:
tags:
    - "guilty_crown"
    - "inori_yuzuriha"
categories:
    - anime
skin_collection:
    - name: "The Everlasting Guilty Crown"
      screenshots:
          - "http://i.imgur.com/IC8hIOL.jpg"
          - "http://i.imgur.com/VyRpPr5.jpg"
          - "http://i.imgur.com/y5g4vlD.png"
          - "http://i.imgur.com/dURxkil.jpg"
          - "http://i.imgur.com/UzZwiHN.png"
          - "http://i.imgur.com/cwdgOYM.jpg"
videos:
    skinship:
    author:
---
