---
layout: skin

skin_name: "eekur"
forum_thread_id: 719208
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 9820272
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - other
skin_collection:
    - name: "eekur"
      screenshots:
          - "https://i.imgur.com/k5dXIKs.jpg"
          - "https://i.imgur.com/yk7vZBu.jpg"
          - "https://i.imgur.com/znSGv2u.png"
          - "https://i.imgur.com/MQLsnV4.jpg"
          - "https://i.imgur.com/riCsHGk.jpg"
          - "https://i.imgur.com/6L5onH8.png"
          - "https://i.imgur.com/wMQDb7X.png"
          - "https://i.imgur.com/7r0mzl3.png"
          - "https://i.imgur.com/WF2K6XM.png"
          - "https://i.imgur.com/9U1LxFI.jpg"
          - "https://i.imgur.com/GDwylOn.jpg"
          - "https://i.imgur.com/G5bPW5D.jpg"
          - "https://i.imgur.com/LFjBPIk.png"
          - "https://i.imgur.com/lFvVhlL.png"
videos:
    skinship:
    author:
---
