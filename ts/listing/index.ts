import { getFullSkinIndex } from "../fetch"
import { initSearch } from "../search/index"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

async function main() {
    const skinIndex = await getFullSkinIndex()

    initSearch({
        skinIndex: skinIndex!,
        pagination: true,
    })
}
