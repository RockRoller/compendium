import { ensuredSelector } from "../shared-dom"
import { Skin } from "../types"
import { searchDebounceTime, storageKeys } from "./config"
import { performSearch, showDefaultListing } from "./search"
import { searchSettings } from "./types"
import { clearUrlQuery } from "./url-query"

/**
 * This function calls all subsequent addListener functions.
 * @param skinIndex skinData[] @see skinData
 */
export function addEventListeners(settings: searchSettings) {
    const searchDebounce = debounce((skinIndex: Skin[]) => {
        performSearch(settings)
    }, searchDebounceTime)
    addClearListener()
    addSearchFoldInListener()
    addVisualsTagsListeners()
    addVisualsGameModeListeners()
    addInputSearchListeners(settings.skinIndex, searchDebounce)
}

// found here, tho there are plenty of very similar ones: https://gist.github.com/fr-ser/ded7690b245223094cd876069456ed6c
export function debounce<F extends Function>(func: F, wait: number): F {
    let timeoutID: number

    // conversion through any necessary as it wont satisfy criteria otherwise
    return <any>function (this: any, ...args: any[]) {
        clearTimeout(timeoutID)
        const context = this

        timeoutID = window.setTimeout(function () {
            func.apply(context, args)
        }, wait)
    }
}

/**
 * This function adds EventListeners to all filter and search items.
 * This Listener triggers the search on click/input
 * @param skinIndex The array of all skins @see skinData
 * @param searchDebounce searchDebounce thing for eventListener (not even gonna pretend I know how it works)
 */
function addInputSearchListeners(skinIndex: Skin[], searchDebounce: Function) {
    ;[
        ...document.querySelectorAll<HTMLDivElement>(".filter-game-mode"),
        ...document.querySelectorAll<HTMLSpanElement>(".filter-tag--cat"),
        ...document.querySelectorAll<HTMLSpanElement>(".filter-tag--resolution"),
        ...document.querySelectorAll<HTMLSpanElement>(".filter-tag--ratio"),
    ].forEach((e) => {
        e.addEventListener("click", () => {
            searchDebounce(skinIndex)
        })
    })
    ;[ensuredSelector<HTMLInputElement>("#search-input"), ensuredSelector<HTMLInputElement>("#date-after"), ensuredSelector<HTMLInputElement>("#date-before")].forEach((e) => {
        e.addEventListener("input", () => {
            searchDebounce(skinIndex)
        })
    })
    ensuredSelector<HTMLSelectElement>(".filter-order-selector").addEventListener("change", () => {
        searchDebounce(skinIndex)
    })
}

/**
 * This function adds EventListeners to all tag filter items.
 * This Listnener changes their visual state on click unselected <=> included <=> excluded
 */
function addVisualsTagsListeners() {
    document.querySelectorAll<HTMLSpanElement>(".filter-tag--cat").forEach((e) => {
        e.addEventListener("click", () => {
            if (e.classList.contains("include")) {
                e.classList.replace("include", "exclude")
            } else if (e.classList.contains("exclude")) {
                e.classList.remove("exclude")
            } else {
                e.classList.add("include")
            }
        })
    })
    document.querySelectorAll<HTMLSpanElement>(".filter-tag--resolution").forEach((e) => {
        e.addEventListener("click", () => e.classList.toggle("include"))
    })
    document.querySelectorAll<HTMLSpanElement>(".filter-tag--ratio").forEach((e) => {
        e.addEventListener("click", () => e.classList.toggle("include"))
    })
}

/**
 * This function add listeners to all game-mode filter items.
 * This Listeners changes their visual state unselected <=> included
 */
function addVisualsGameModeListeners() {
    document.querySelectorAll(".filter-game-mode").forEach((e) => {
        e.addEventListener("click", () => {
            e.classList.contains("include") ? e.classList.remove("include") : e.classList.add("include")
        })
    })
}

/**
 * This function adds an EventListener to the trash button on the skin listing.
 * This Listener clears the search and filters on click.
 */
function addClearListener() {
    ensuredSelector(".trash").addEventListener("click", () => {
        // reset search
        document.querySelector<HTMLInputElement>("#search-input")!.value = ""
        document.querySelector<HTMLOptionElement>(".filter-order-selector option")!.selected = true

        // reset dates
        ensuredSelector<HTMLInputElement>("#date-after").value = ""
        ensuredSelector<HTMLInputElement>("#date-before").value = ""

        clearUrlQuery()
        showDefaultListing()

        document.querySelectorAll(".include").forEach((e) => e.classList.remove("include"))
        document.querySelectorAll(".exclude").forEach((e) => e.classList.remove("exclude"))
    })
}

/**
 * This function adds an EventListener to the chevron button on the skin listing.
 * This button fold-in/-out the search
 */
function addSearchFoldInListener() {
    ensuredSelector<HTMLElement>(".search .chevron-down").addEventListener("click", () => {
        ensuredSelector<HTMLElement>(".filters").classList.toggle("folded")
        const setting = localStorage.getItem(storageKeys.searchFolded)
        localStorage.setItem(storageKeys.searchFolded, setting == "true" ? "false" : "true")
    })
}

/**
 * This function reads the state the search should have from localstorage and adjusts the search accordingly
 */
export function readSearchState() {
    if (localStorage.getItem(storageKeys.searchFolded) == null) {
        localStorage.setItem(storageKeys.searchFolded, "true")
    }

    const extended = localStorage.getItem(storageKeys.searchFolded)
    if (extended == "false") {
        ensuredSelector<HTMLElement>(".filters").classList.add("folded")
    }
}
