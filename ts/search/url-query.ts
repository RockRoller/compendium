import { ensuredSelector } from "../shared-dom"
import { GameModes, Ratios, Resolutions } from "../types"
import { searchDates } from "./config"
import { isFilterDefault, performSearch } from "./search"
import { filter, searchSettings, sortOrder } from "./types"

/**
 * This function sets the query to the given filter
 * @param filter @see filter
 */
export function setUrlQueryParameters(filter: filter) {
    if (isFilterDefault(filter)) {
        clearUrlQuery()
        return
    }

    let newUrl = window.location.pathname.concat("?", generateUrlQueryParameter(filter).toString())

    window.history.pushState(null, document.title, newUrl)
    window.history.replaceState(null, document.title, newUrl)
}

/**
 * This function generates the url for a given filter
 * @param filter the filter to create the query param for
 * @returns URLSearchParams
 */
export function generateUrlQueryParameter(filter: filter) {
    const searchParam = new URLSearchParams()

    if (filter.order != "new-old") {
        searchParam.append("order", filter.order)
    }
    if (filter.gameModeInclude.length != 0) {
        searchParam.append("modes", filter.gameModeInclude.join("+"))
    }
    if (filter.strInclude.length != 0) {
        searchParam.append("search", filter.strInclude.join("+"))
    }
    if (filter.catExclude.length != 0) {
        searchParam.append("catex", filter.catExclude.join("+"))
    }
    if (filter.catInclude.length != 0) {
        searchParam.append("catin", filter.catInclude.join("+"))
    }
    if (filter.resolutions.length != 0) {
        searchParam.append("res", filter.resolutions.join("+"))
    }
    if (filter.ratios.length != 0) {
        searchParam.append("ratio", filter.ratios.join("+"))
    }
    if (filter.dateAfter.getTime() != searchDates.oldest.getTime()) {
        searchParam.append("date-after", filter.dateAfter.getTime().toString())
    }
    if (filter.dateBefore.getTime() != searchDates.newest.getTime()) {
        searchParam.append("date-before", filter.dateBefore.getTime().toString())
    }

    return searchParam
}

/**
 * This function updates for the query which page of the results is shown
 * @param page page number (index + 1 because humans start counting at 1!)
 */
export function updateUrlQueryPage(page: number) {
    const searchParam = new URLSearchParams(window.location.search)
    const pageKey = "p"

    if (searchParam.has(pageKey)) {
        if (page != 1) {
            searchParam.set(pageKey, page.toString())
        } else {
            searchParam.delete(pageKey)
        }
    } else if (page != 1) {
        // this adds the key if missing, unless its page 1, in which case we skip
        searchParam.append(pageKey, page.toString())
    }

    let newUrl = window.location.pathname.concat("?", searchParam.toString())
    if (newUrl.endsWith("?")) {
        newUrl = newUrl.replace("?", "")
    }
    window.history.pushState(null, document.title, newUrl)
    window.history.replaceState(null, document.title, newUrl)
}

/**
 * This function clears the entire query string
 */
export function clearUrlQuery() {
    window.history.pushState(null, document.title, window.location.pathname)
    window.history.replaceState(null, document.title, window.location.pathname)
}

/**
 * This function reads the query from the url and sets the filters accordingly
 * @returns filter from url @see filter and page
 */
export function readUrlQuery() {
    const searchParam = new URLSearchParams(window.location.search)
    let filter: filter = {
        strInclude: [],
        gameModeInclude: [],
        catInclude: [],
        catExclude: [],
        resolutions: [],
        ratios: [],
        order: ensuredSelector<HTMLSelectElement>(".filter-order-selector").value as sortOrder,
        dateAfter: searchDates.oldest,
        dateBefore: searchDates.newest,
    }
    let page = 1

    // categories that have since been removed
    const legacyCategories = ["vtuber", "love_live", "vocaloid", "touhou", "azur_lane", "kpop"]

    searchParam.forEach((value, key) => {
        switch (key) {
            case "order":
                filter.order = value as sortOrder
                break
            case "res":
                value.split("+").forEach((e) => filter.resolutions!.push(e as Resolutions))
                break
            case "ratio":
                value.split("+").forEach((e) => filter.ratios!.push(e as Ratios))
                break
            case "search":
                value.split("+").forEach((e) => filter.strInclude!.push(e))
                break
            // tagex was used before catex, legacy compatability
            case "catex":
            case "tagex":
                value.split("+").forEach((e) => {
                    if (!legacyCategories.includes(e)) {
                        filter.catExclude!.push(e)
                    }
                })
                break
            // tagin was used before catex, legacy compatability
            case "catin":
            case "tagin":
                value.split("+").forEach((e) => {
                    if (!legacyCategories.includes(e)) {
                        filter.catInclude!.push(e)
                    } else {
                        if (!filter.strInclude) {
                            filter.strInclude = []
                        }
                        filter.strInclude!.push(e)
                    }
                })
                break
            case "modes":
                value.split("+").forEach((e) => filter.gameModeInclude!.push(e as GameModes))
                break
            case "date-before":
                filter.dateBefore = new Date(parseInt(value))
                break
            case "date-after":
                filter.dateAfter = new Date(parseInt(value))
                break
        }
    })
    if (searchParam.has("p")) {
        page = parseInt(searchParam.get("p")!)
    }

    return [filter, page] as const
}

/**
 * This function uses the query read in readUrlQuery() and sets visuals accordingly with a call to performSearch()
 * @param filter filter to use from
 * @param skinIndex A list of skins to be filtered
 * @param page page number (index + 1 because humans start counting at 1!)
 */
export function useReadQuery(filter: filter, page: number, settings: searchSettings) {
    const searchInput = document.querySelector<HTMLInputElement>("#search-input")!
    const tagIncludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--cat")
    const tagExcludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--cat")
    const resolutionIncludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--resolution")
    const ratioIncludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--ratio")
    const gameModeIncludeNodes = document.querySelectorAll<HTMLDivElement>(".filter-game-mode")
    const orderSelector = document.querySelector<HTMLSelectElement>(".filter-order-selector")!

    searchInput.value = filter.strInclude.join(" ")

    filter.gameModeInclude.forEach((e) => {
        gameModeIncludeNodes.forEach((g) => {
            if (g.dataset.tag == e) {
                g.classList.add("include")
            }
        })
    })
    filter.resolutions.forEach((e) => {
        resolutionIncludeNodes.forEach((r) => {
            if (r.dataset.tag == e) {
                r.classList.add("include")
            }
        })
    })
    filter.ratios.forEach((e) => {
        ratioIncludeNodes.forEach((r) => {
            if (r.dataset.tag == e) {
                r.classList.add("include")
            }
        })
    })
    filter.catInclude.forEach((e) => {
        tagIncludeNodes.forEach((t) => {
            if (t.dataset.tag == e) {
                t.classList.add("include")
            }
        })
    })
    filter.catExclude.forEach((e) => {
        tagExcludeNodes.forEach((t) => {
            if (t.dataset.tag == e) {
                t.classList.add("exclude")
            }
        })
    })
    if (filter.order != "new-old") {
        orderSelector.querySelector<HTMLOptionElement>(`option[value=${filter.order}]`)!.selected = true
    }
    if (filter.dateAfter.getTime() != searchDates.oldest.getTime()) {
        ensuredSelector<HTMLInputElement>("#date-after").value = filter.dateAfter.toISOString().split("T")[0]
    }
    if (filter.dateBefore.getTime() != searchDates.newest.getTime()) {
        ensuredSelector<HTMLInputElement>("#date-before").value = filter.dateBefore.toISOString().split("T")[0]
    }
    performSearch(settings, page)
}
