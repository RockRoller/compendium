/**
 * This type defines all game modes existing in osu! stable
 */
export type GameModes = "standard" | "mania" | "catch" | "taiko"

/**
 * This type defines all asset resolutions that osu! supports
 */
export type Resolutions = "hd" | "sd"

/**
 * This type defines all aspect ratios that the compendium supports
 */
export type Ratios = "all" | "16:9" | "16:10" | "4:3" | "21:9"

/**
 * Defines a skin as it is read-in from search.json
 * This type defines the structure of a skin. @see formats.md
 */
export type Skin = {
    skin_name: string
    forum_thread_id: number
    date_added: Date
    date_released: Date
    url: string
    game_modes: GameModes[]
    author: string[]
    formatted_author: string
    resolutions: Resolutions[]
    ratios: Ratios[]
    tags: string[]
    categories: string[]
    skin_collection: SkinVersion[]
}

export type SkinVersion = {
    name: string
    screenshots: string[]
}
