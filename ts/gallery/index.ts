import { ensuredSelector } from "../shared-dom"
import { Gallery } from "./components/Gallery"
import { skinInformation } from "./types"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

function main() {
    // create gallery and replace jekyll gallery
    const gallery = new Gallery(getSkinData())
    ensuredSelector(".gallery").replaceWith(gallery)
    // switching of displayed variant
    const skinSelector = document.querySelector<HTMLSelectElement>(".skin-selector")
    if (skinSelector !== null) {
        skinSelector.addEventListener("change", () => {
            gallery.switchVariant(parseInt(skinSelector.value))

            // remove focus from selector
            if (document.activeElement instanceof HTMLElement) {
                document.activeElement.blur()
            }
        })
    }
}

/**
 * fetch the list of variants from the document
 * @returns
 */
function getSkinData() {
    return JSON.parse(ensuredSelector<HTMLScriptElement>("#json-collection").innerText, function (key, value) {
        if (key == "skinship" || key == "author") {
            if (value == null) {
                return []
            }
        }
        return value
    }) as skinInformation
}
