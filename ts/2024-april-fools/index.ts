import { getFullSkinIndex } from "../fetch"
import { createElement, ensuredSelector } from "../shared-dom"
import { CoinsController } from "./CoinsController"
import { GachaSelector } from "./components/GachaSelector"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

async function main() {
    const coinsController = new CoinsController()
    const target = ensuredSelector("#gacha-root")

    const allSkins = (await getFullSkinIndex()) ?? []

    target.append(new GachaSelector(allSkins, coinsController))

    const purseLabel = createElement("span", {
        className: "coin-purse__label",
    })
    updatePurseLabel()

    target.prepend(
        createElement("div", {
            className: "coin-purse",
            children: [
                createElement("img", {
                    className: "coin-purse__img",
                    attributes: {
                        src: "/assets/img/gacha/coins.webp",
                    },
                }),
                purseLabel,
            ],
        })
    )

    const channel = new BroadcastChannel("coins_channel")

    channel.addEventListener("message", () => {
        updatePurseLabel()
    })

    document.addEventListener("click", (e: MouseEvent) => {
        const coins = coinsController.increaseCoinsRandomly(1)

        const x = e.pageX
        const y = e.pageY

        const popup = createElement("div", {
            className: "coin-popup",
            children: [
                createElement("img", {
                    className: "coin-popup__coin",
                    attributes: {
                        src: "/assets/img/gacha/coins.webp",
                    },
                }),
                createElement("span", {
                    className: "coin-popup__label",
                    attributes: {
                        innerText: `+${coins}`,
                    },
                }),
            ],
            style: {
                top: `${y}px`,
                left: `${x}px`,
            },
        })

        document.body.append(popup)
        setTimeout(() => {
            popup.remove()
        }, 5000)
    })

    function updatePurseLabel() {
        purseLabel.innerText = `${coinsController.getCoins()} osu!coins`
    }
}
