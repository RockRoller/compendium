import { SkinItem } from "../../search/components/skin-item"
import { wait } from "../../shared"
import { createElement, createIcon } from "../../shared-dom"
import { Skin } from "../../types"
import { CoinsController } from "../CoinsController"
import { GachaController } from "../GachaController"
import { GachaSelector } from "./GachaSelector"

export class BannerMenu extends HTMLElement {
    private pullLabel = createElement("span", {
        className: "banner-menu__title-total-pulls",
    })

    private pity4Label = createElement("span", {
        className: "banner-menu__title-pity-label banner-menu__title-pity-label--4",
    })

    private pity5Label = createElement("span", {
        className: "banner-menu__title-pity-label banner-menu__title-pity-label--5",
    })

    private resultGrid = createElement("div", {
        className: "skin-grid",
        children: [],
    })

    constructor(private controller: GachaController, allSkins: Skin[], gameController: CoinsController) {
        super()

        this.updateTitleLabels()

        this.classList.add("banner-menu")

        this.append(
            createElement("button", {
                className: "button button--secondary",
                attributes: {
                    innerText: "<= back to gacha selector",
                    onclick: () => {
                        this.replaceWith(new GachaSelector(allSkins, gameController))
                    },
                },
            })
        )

        this.append(
            createElement("div", {
                className: "banner-menu__title",
                children: [
                    createElement("span", { className: "banner-menu__title-label", attributes: { innerText: controller.getGachaName() } }),
                    createElement("div", { className: "banner-menu__title-stats", children: [this.pullLabel, this.pity4Label, this.pity5Label] }),
                ],
            })
        )
        this.append(this.getRarityDisplaySection(5))
        this.append(this.getRarityDisplaySection(4))
        this.append(this.getRarityDisplaySection(3))

        const pullButton1 = createElement("button", {
            className: "banner-menu__pull-button button button--primary",
            attributes: {
                innerText: "spend 100 osu!coins to perform 1 pulls",
            },
        })
        const pullButton10 = createElement("button", {
            className: "banner-menu__pull-button button button--primary",
            attributes: {
                innerText: "spend 727 osu!coins to perform 10 pulls",
            },
        })

        const pull = async (times: number, coins: number) => {
            if (coins > gameController.getCoins()) {
                window.alert("You do not have enough coins for this action! Please obtain more before trying again")
                return
            } else {
                gameController.spendCoins(coins)
                const pulls = []
                for (let i = 0; i < times; i++) {
                    await wait(50)
                    pulls.push(this.controller.pull())
                }
                this.updateTitleLabels()
                this.resultGrid.innerHTML = ""
                pulls.forEach((pull) => {
                    const item = new SkinItem(pull)
                    item.classList.add(`skin-item--rarity-${controller.getSkinRarity(pull)}`)
                    this.resultGrid.prepend(item)
                })
            }
        }

        pullButton1.addEventListener("click", () => {
            pull(1, 100)
        })

        pullButton10.addEventListener("click", async () => {
            pull(10, 727)
        })

        this.append(pullButton1, pullButton10)

        this.append(this.resultGrid)
    }

    updateTitleLabels() {
        this.pullLabel.innerText = "total pulls: " + this.controller.getTotalPulls().toString()
        this.pity4Label.innerText = `4* pity (${this.controller.getPity(4).toString()}/${this.controller.hard4})`
        this.pity5Label.innerText = `5* pity (${this.controller.getPity(5).toString()}/${this.controller.hard5})`
    }

    getRarityDisplaySection(rarity: 3 | 4 | 5) {
        const menu = createElement("div", {
            className: `skin-preview skin-preview--${rarity}`,
            children: [
                createElement("span", {
                    className: "skin-preview-label",
                    attributes: {
                        innerText: `${this.controller.getSkins(rarity).length} different ${rarity}* skins`,
                    },
                }),
            ],
        })

        if (rarity != 3) {
            menu.append(
                createElement("div", {
                    className: "skin-preview-list",
                    children: this.controller.getSkins(rarity).map((skin) =>
                        createElement("div", {
                            className: "skin-preview-item",
                            children: [
                                createElement("img", { className: "skin-preview-item__cover", attributes: { src: `/assets/img/covers/${skin.forum_thread_id}.webp` } }),
                                createElement("div", {
                                    className: "skin-preview-item__title",
                                    children: [createElement("span", { attributes: { innerText: skin.skin_name } }), ...skin.game_modes.map((mode) => createIcon(mode == "catch" ? "ctb" : mode))],
                                }),
                                createElement("span", { className: "skin-preview-item__author", attributes: { innerText: skin.formatted_author } }),
                            ],
                        })
                    ),
                })
            )
        }

        return menu
    }
}

customElements.define("banner-menu", BannerMenu)
