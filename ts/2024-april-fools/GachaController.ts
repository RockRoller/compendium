import { Skin } from "../types"
import { saveItemToSkinCollection } from "./skin-collection-helper"

export class GachaController {
    private skins3: Skin[] = []
    private skins4: Skin[] = []
    private skins5: Skin[] = []

    // odds to get a skin of that rarity (0.0 - 1.0)
    private odds4 = 0.1
    private odds5 = 0.02
    // how many pulls without a pull of this rarity (or higher) to trigger hard pity
    hard4 = 10
    hard5 = 50

    constructor(skins: Skin[], private bannerName: string, private bannerKey: string) {
        for (const skin of skins) {
            switch (this.getSkinRarity(skin)) {
                case 5:
                    this.skins5.push(skin)
                    break
                case 4:
                    this.skins4.push(skin)
                    break
                case 3:
                    this.skins3.push(skin)
                    break
            }
        }
    }

    getTotalPulls() {
        return parseInt(localStorage.getItem(`${this.bannerKey}-total`) ?? "0")
    }

    getGachaName() {
        return this.bannerName
    }

    getPityKey(rarity: 4 | 5) {
        return `${this.bannerKey}-pity-${rarity}`
    }

    getPity(rarity: 4 | 5) {
        return parseInt(localStorage.getItem(this.getPityKey(rarity)) ?? "0")
    }

    getSkins(rarity: 3 | 4 | 5) {
        switch (rarity) {
            case 3:
                return this.skins3
            case 4:
                return this.skins4
            case 5:
                return this.skins5
        }
    }

    pull(): Skin {
        let pity4 = this.getPity(4)
        let pity5 = this.getPity(5)

        const pick4 = () => {
            pity4 = 0
            pull = pickRandom(this.skins4)
        }

        const pick5 = () => {
            pity4 = 0
            pity5 = 0
            pull = pickRandom(this.skins5)
        }

        // 3* is assigned here rather than in the if() to prevent JS from thinking this can return undefined...
        // offsetting pity by one to guarantee "1 in X" rather than "next after X fails"
        let pull = pickRandom(this.skins3)
        const roll = Math.random()
        if (pity5 >= this.hard5 - 1) {
            pick5()
        } else if (pity4 >= this.hard4 - 1) {
            if (roll < this.odds5) {
                pick5()
                pity5 = 0
                pity4 = 0
            } else {
                pick4()
            }
        } else {
            if (roll < this.odds5) {
                pick5()
                pity5 = 0
                pity4 = 0
            } else if (roll < this.odds4 + this.odds5) {
                // adding odds5 to fix overlaping roll ranges
                pick4()
                pity4 = 0
                pity5++
            } else {
                pity4++
                pity5++
            }
        }

        localStorage.setItem(this.getPityKey(4), pity4.toString())
        localStorage.setItem(this.getPityKey(5), pity5.toString())
        localStorage.setItem(`${this.bannerKey}-total`, (this.getTotalPulls() + 1).toString())

        saveItemToSkinCollection(pull.forum_thread_id)

        return pull

        function pickRandom(skins: Skin[]) {
            return skins[Math.floor(Math.random() * skins.length)]
        }
    }

    getSkinRarity(skin: Skin) {
        // contest submission | soty top 10 | bulletin entry
        const tags4 = ["contest_submission", /skin_of_the_year_\d{4}_top_10/gm]
        // contest top 3 | soty top 3
        const tags5 = [/contest_\d{1,2}_(winner|2nd_place|3rd_place)/gm, /skin_of_the_year_\d{4}_(winner|2nd_place|3rd_place)/gm]
        // check 5
        for (const tag of tags5) {
            for (const skinTag of skin.tags) {
                if (skinTag.match(tag)) {
                    return 5
                }
            }
        }
        // check 4
        for (const tag of tags4) {
            for (const skinTag of skin.tags) {
                if (skinTag.match(tag)) {
                    return 4
                }
            }
        }

        return 3
    }
}
